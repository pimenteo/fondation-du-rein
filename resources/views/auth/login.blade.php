@extends('layout')

@section('css')
    <link rel="stylesheet" href="{{ url('front/css/authentification.css') }}">
@endsection

@section('content')

    <div class="container-fluid">

        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="clearfix text-center panel">
                    <a id="logo" class="logo" href="{{ url('/') }}">
                        <span class="logo_image">
                            <img alt="Logo" src="{{ url('front/img/logo.png') }}"/>
                        </span>
                    </a>
                </div>
                
                <div class="panel panel-default">
                    <h1 class="panel-heading">Authentification</h1>
                    <div class="panel-body">
                        @if (session('status'))
                            <div class="success-message">
                                {{ session('status') }}
                            </div>
                        @endif
                        <form class="form-horizontal" method="POST">
                            {!! csrf_field() !!}

                            <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                                <label class="col-md-4 control-label">Adresse e-mail :</label>
                                <div class="col-md-6">
                                    <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                                    <span class="help-block">
                                        {{ $errors->first('email') }}
                                    </span>
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                                <label class="col-md-4 control-label">Mot de passe :</label>
                                <div class="col-md-6">
                                    <input type="password" class="form-control" name="password">
                                    <span class="help-block">
                                        {{ $errors->first('password') }}
                                    </span>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" class="form-control" name="remember"> <span>Se souvenir de moi</span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-3">
                                    <button type="submit" class="btn btn-primary button button-medium center-block">S'authentifier</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection
