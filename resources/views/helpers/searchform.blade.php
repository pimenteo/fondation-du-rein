
@if (isset($searchfields) && !empty($searchfields))
	<div class="card">
		<div class="header clearfix">
			<div class="pull-left">
            	<h4 class="title">Rechercher</h4>
            </div>
        </div>

        <div id="search-and-order" class="closed">

    		{!! Form::open(['url' => $actionUrl, 'class' => 'clearfix']) !!}
				<div id="sao-container" class="panel clearfix">

					<div id="searchForm" class="{{ isset($orderfields) && !empty($orderfields) ? 'col-md-6' : 'col-md-12' }}">
						@foreach ($searchfields as $fieldName => $props)

							@if ($props['type'] == 'like' && !isset($props['options']))
								<div class="row {{ isset($orderfields) && !empty($orderfields) ? '' : ' col-md-6' }}">
			                		<div class="form-group">
			                			{!! Form::label($fieldName, $props['label'], ['class' => 'col-md-3 control-label text-right']) !!}
			                			<div class="col-md-9">
									        {!! Form::text('search['.$fieldName.']['.$props['type'].']', $props['value'], ['class' => 'form-control', 'id' => $fieldName]) !!}
							    		</div>
							    	</div>
							    </div>
					    	@elseif ($props['type'] == 'equal' && !isset($props['options']))
					    		<div class="row {{ isset($orderfields) && !empty($orderfields) ? '' : ' col-md-6' }}">
			                		<div class="form-group">
			                			{!! Form::label($fieldName, $props['label'], ['class' => 'col-md-3 control-label text-right']) !!}
			                			<div class="col-md-9">
									        {!! Form::text('search['.$fieldName.']['.$props['type'].']', $props['value'], ['class' => 'form-control', 'id' => $fieldName]) !!}
							    		</div>
							    	</div>
							    </div>
					    	@elseif ($props['type'] == 'equal' && !empty($props['options']))
					    		<div class="row {{ isset($orderfields) && !empty($orderfields) ? '' : ' col-md-6' }}">
			                		<div class="form-group">
			                			{!! Form::label($fieldName, $props['label'], ['class' => 'col-md-3 control-label text-right']) !!}
			                			<div class="col-md-9">
			                				{!! Form::select('search['.$fieldName.']['.$props['type'].']', $props['options'], $props['value'], ['class' => 'form-control', 'id' => $fieldName]) !!}
		                				</div>
							    	</div>
							    </div>
					    	@elseif ($props['type'] == 'between-dates')
							    <div class="row {{ isset($orderfields) && !empty($orderfields) ? '' : ' col-md-6' }}">
			                		<div class="form-group">
			                			{!! Form::label($fieldName, $props['label'], ['class' => 'col-md-3 control-label text-right']) !!}
			                			<div class="col-md-4">
					    					{!! Form::text('search['.$fieldName.']['.$props['type'].'][min]', null, ['class' => 'form-control datepicker', 'id' => $fieldName]) !!}
		                				</div>
			                			<div class="col-md-4">
											{!! Form::text('search['.$fieldName.']['.$props['type'].'][max]', null, ['class' => 'form-control datepicker', 'id' => $fieldName]) !!}
		                				</div>
							    	</div>
							    </div>
					    	@elseif ($props['type'] == 'between')
					    		<div class="row {{ isset($orderfields) && !empty($orderfields) ? '' : ' col-md-6' }}">
			                		<div class="form-group">
			                			{!! Form::label($fieldName, $props['label'], ['class' => 'col-md-3 control-label text-right']) !!}
			                			<div class="col-md-4">
					    					{!! Form::text('search['.$fieldName.']['.$props['type'].'][min]', null, ['class' => 'form-control', 'id' => $fieldName]) !!}
		                				</div>
			                			<div class="col-md-4">
											{!! Form::text('search['.$fieldName.']['.$props['type'].'][max]', null, ['class' => 'form-control', 'id' => $fieldName]) !!}
		                				</div>
							    	</div>
							    </div>
					    	@elseif ($props['type'] == 'bool')
					    		<div class="row {{ isset($orderfields) && !empty($orderfields) ? '' : ' col-md-6' }}">
			                		<div class="form-group">
			                			{!! Form::label($fieldName, $props['label'], ['class' => 'col-md-3 control-label text-right']) !!}
			                			<div class="col-md-9">
			                				{!! Form::select('search['.$fieldName.']['.$props['type'].']', ['' => 'Tous', 0 => 'Non', 1 => 'Oui'], $props['value'], ['class' => 'form-control', 'id' => $fieldName]) !!}
		                				</div>
							    	</div>
							    </div>
							@endif

						@endforeach
					</div>

					<div id="orderForm" class="col-md-6">

						@if (isset($orderfields) && !empty($orderfields))
							@foreach ($orderfields as $fieldName => $props)
								<div class="row">
			                		<div class="form-group">
			                			{!! Form::label($fieldName, $props['label'], ['class' => 'col-md-3 control-label text-right']) !!}
			                			<div class="col-md-9">
			                				{!! Form::select($fieldName, $props['options'], $props['value'], ['class' => 'form-control', 'id' => $fieldName]) !!}
		                				</div>
							    	</div>
							    </div>
					    	@endforeach
					    @endif
					</div>

					<div class="clearfix">
						<div class="col-md-12 text-right">
							@if (isset($submitButtons) && !empty($submitButtons))
								{!! Form::reset('Reset', ['class' => 'btn btn-default btn-fill']) !!}
								@foreach ($submitButtons as $value => $options)
									{!! Form::button($value, $options) !!}
								@endforeach
							@else
								{!! Form::reset('Reset', ['class' => 'btn btn-default btn-fill']) !!}
								{!! Form::submit('Rechercher', ['class' => 'btn btn-primary btn-fill']) !!}
							@endif
						</div>
					</div>

				</div>

			{!! Form::close() !!}

        </div>
	</div>
@endif
