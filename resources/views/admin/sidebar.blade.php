<div class="sidebar" data-color="grey" data-image="assets/img/sidebar-5.jpg">

    <div class="sidebar-wrapper">
        <div class="logo">
            <a href="{{ url('/') }}" class="simple-text">
                <img alt="Logo" src="{{ url('front/img/logo.png') }}" class="img-responsive"/>
            </a>
        </div>

        <ul class="nav">
            <li class="{{ isset($bodyId) && $bodyId == 'dashboard' ? 'active' : '' }}">
                <a href="{{ url('/statistiques') }}">
                    <i class="fa fa-area-chart"></i>
                    <p>Statistiques</p>
                </a>
            </li>
            <li class="{{ isset($bodyId) && $bodyId == 'statsdons' ? 'active' : '' }}">
                <a href="{{ url('/statistiques/dons/search') }}">
                    <i class="fa fa-gift"></i>
                    <p>Statistiques dons</p>
                </a>
            </li>
            <li class="{{ isset($bodyId) && $bodyId == 'contacts' ? 'active' : '' }}">
                <a href="{{ url('/contacts') }}">
                    <i class="fa fa-address-card-o"></i>
                    <p>Contacts</p>
                </a>
            </li>
            <li class="{{ isset($bodyId) && $bodyId == 'evenements' ? 'active' : '' }}">
                <a href="{{ url('/evenements') }}">
                    <i class="fa fa-calendar"></i>
                    <p>événements</p>
                </a>
            </li>
            <li class="{{ isset($bodyId) && $bodyId == 'administrators' ? 'active' : '' }}">
                <a href="{{ url('/administrators') }}">
                    <i class="fa fa-user-circle-o"></i>
                    <p>Administrateurs</p>
                </a>
            </li>
        </ul>
    </div>

</div>
