<nav class="navbar navbar-default navbar-fixed">
    <div class="container-fluid">

		@if (Auth::check())
			<div class="navbar-header">
	            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
	                <span class="sr-only">Toggle navigation</span>
	                <span class="icon-bar"></span>
	                <span class="icon-bar"></span>
	                <span class="icon-bar"></span>
	            </button>

	            @yield('breadcrumb')
	        </div>
	        <div class="collapse navbar-collapse">
	            <ul class="nav navbar-nav navbar-right">
	                <li>
	                    <a href="{{ url('auth/logout') }}">
	                        Déconnexion
	                    </a>
	                </li>
	            </ul>
	        </div>
	    </div>
	@endif
</nav>
