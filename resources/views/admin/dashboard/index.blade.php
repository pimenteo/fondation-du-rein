@extends('admin.layout')

@section('breadcrumb')
	<a class="navbar-brand" href="#">Tableau de bord</a>
@endsection

@section('searchform')
	{!! $searchForm or '' !!}
@endsection

@section('content')

	<div class="col-md-12">

		@yield('searchform')

		<div class="card">
			<div class="header">
				<h4 class="title">Statistiques des contacts</h4>
			</div>

			<div class="content table-responsive table-full-width">

				<table class="table table-hover table-striped table-hover table-condensed">
					<thead>
						<tr>
							<th>Nom complet</th>
	                    	<th>Invitation envoyée</th>
	                    	<th>Participation confirmée</th>
							<th>Total des dons</th>
						</tr>
					</thead>
					@if (!empty($datas))
						<tbody>
							@foreach ($datas as $contact)
								<tr>
									<td>{{ $contact->full_name }}</td>
									<td>{{ $contact->invited_in_last_gala }}</td>
		                        	<td>{{ $contact->confirmed_in_last_gala }}</td>
									<td>{{ $contact->total_dons }}</td>
								</tr>
							@endforeach
						</tbody>
					@endif
				</table>

				@if ($query->lastPage() > 1)
                <ul class="pagination pull-right">
				    <li class="{{ ($query->currentPage() == 1) ? ' disabled' : '' }}">
				        <a href="{{ $query->url(1) }}"><i class="fa fa-chevron-left"></i></a>
				    </li>
	                @for ($i = $initial = max(1, $query->currentPage() - 5), $l = min($initial + 10, $query->lastPage()); $i <= $l; $i++)
	                	<li class="{{ ($query->currentPage() == $i) ? ' active' : '' }}">
				            <a href="{{ $query->url($i) }}">{{ $i }}</a>
				        </li>
					@endfor
					<li class="{{ ($query->currentPage() == $query->lastPage()) ? ' disabled' : '' }}">
				        <a href="{{ $query->url($query->currentPage()+1) }}" ><i class="fa fa-chevron-right"></i></a>
				    </li>
				</ul>
				@endif
				
			</div>
		</div>
		
	</div>

@endsection
