@extends('admin.layout')

@section('breadcrumb')
	<a class="navbar-brand" href="#">Statistiques des dons</a>
@endsection

@section('searchform')
	{!! $searchForm or '' !!}
@endsection

@section('content')

	<div class="col-md-12">

		@yield('searchform')

		<div class="card">
			<div class="header">
				<h4 class="title">Statistiques des dons</h4>
			</div>

			<div class="content table-responsive table-full-width">

				<table class="table table-hover table-striped table-hover table-condensed">
					<thead>
						<tr>
							<th>Date</th>
							<th>Contact</th>
							<th>Montant</th>
							<th>Moyen de paiement</th>
						</tr>
					</thead>
					@if (!empty($datas))
						<tbody>
							@foreach ($datas as $don)
								<tr>
									<td>{{ $don->formatted_date_don }}</td>
									<td>{{ $don->contact ? $don->contact->full_name : '' }}</td>
									<td>{{ $don->formatted_amount }}</td>
									<td>{{ $don->payment }}</td>
								</tr>
							@endforeach
						</tbody>
					@endif
				</table>

				@if ($query->lastPage() > 1)
                <ul class="pagination pull-right">
				    <li class="{{ ($query->currentPage() == 1) ? ' disabled' : '' }}">
				        <a href="{{ $query->url(1) }}"><i class="fa fa-chevron-left"></i></a>
				    </li>
	                @for ($i = $initial = max(1, $query->currentPage() - 5), $l = min($initial + 10, $query->lastPage()); $i <= $l; $i++)
	                	<li class="{{ ($query->currentPage() == $i) ? ' active' : '' }}">
				            <a href="{{ $query->url($i) }}">{{ $i }}</a>
				        </li>
					@endfor
					<li class="{{ ($query->currentPage() == $query->lastPage()) ? ' disabled' : '' }}">
				        <a href="{{ $query->url($query->currentPage()+1) }}" ><i class="fa fa-chevron-right"></i></a>
				    </li>
				</ul>
				@endif
				
			</div>
		</div>
		
	</div>

@endsection