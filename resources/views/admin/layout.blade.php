<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

        <script src="{{ url('back/private.built.js') }}" type="text/javascript"></script>
        <script src="{{ url('back/public.built.js') }}" type="text/javascript"></script>
    </head>

    <body>
        @include('admin.sidebar')

        <div class="main-panel">
            @include('admin.header-menu')

             <div class="content">
                <div class="container-fluid">

                    @include('helpers.messagebag')

                    @yield('content')
                </div>
            </div>
        </div>
        
    </body>
</html>
