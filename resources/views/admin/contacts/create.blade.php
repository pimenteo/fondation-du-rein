@extends('admin.layout')

@section('breadcrumb')
	<a class="navbar-brand" href="{{ url('contacts') }}">Contacts</a>
	<span class="navbar-brand"> | </span>
	<a class="navbar-brand" href="#">Ajouter un contact</a>
@endsection

@section('content')

	<div class="col-md-12">
		<div class="card">
	        <div class="header">
	            <h4 class="title">Ajouter un contact</h4>
	        </div>
	        <div class="content contact">
	        	{!! Form::model($model, ['route' => 'contacts.store', 'class' => 'clearfix']) !!}

	        		<div class="col-md-6">
	        			<div class="header">
	        				<legend class="title">Contact</legend>
		        		</div>

		        		<div class="content">
		        			<div class="row">
		        				<div class="form-group">
							        {!! Form::label('civility', 'Civilité', ['class' => 'col-md-3 control-label text-right']) !!}
		        					<div class="col-md-9">
					        			{!! Form::select('civility', $model->getCivilityTypes(), null, ['class' => 'form-control']) !!}
						    		</div>
						    	</div>
						    	
		        			</div>
		        			<div class="row free_civility">
		        				<div class="form-group">
							        {!! Form::label('free_civility', 'Autre civilité', ['class' => 'col-md-3 control-label text-right']) !!}
		        					<div class="col-md-9">
					        			{!! Form::text('free_civility', null, ['class' => 'form-control']) !!}
						    		</div>
						    	</div>
		        			</div>
		        			<div class="row">
		                		<div class="form-group">
							        {!! Form::label('particule', 'Particule', ['class' => 'col-md-3 control-label text-right']) !!}
							        <div class="col-md-9">
								        {!! Form::text('particule', null, ['class' => 'form-control']) !!}
							        </div>
							    </div>
					        </div>
					        <div class="row">
		                		<div class="form-group">
							        {!! Form::label('firstname', 'Nom', ['class' => 'col-md-3 control-label text-right']) !!}
							        <div class="col-md-9">
								        {!! Form::text('firstname', null, ['class' => 'form-control']) !!}
							        </div>
							    </div>
					        </div>
					        <div class="row">
		                		<div class="form-group">
							        {!! Form::label('lastname', 'Prénom', ['class' => 'col-md-3 control-label text-right']) !!}
							        <div class="col-md-9">
								        {!! Form::text('lastname', null, ['class' => 'form-control']) !!}
							        </div>
							    </div>
					        </div>
					        <div class="row">
		                		<div class="form-group">
							        {!! Form::label('email', 'Adresse E-mail', ['class' => 'col-md-3 control-label text-right']) !!}
							        <div class="col-md-9">
							        	{!! Form::text('email', null, ['class' => 'form-control']) !!}
							    	</div>
							    </div>
					        </div>
					        <div class="row">
		                		<div class="form-group">
		                			{!! Form::label('telephone_1', 'Tél. principal', ['class' => 'col-md-3 control-label text-right']) !!}
		                			<div class="col-md-9">
							        	{!! Form::text('telephone_1', null, ['class' => 'form-control']) !!}
						    		</div>
						    	</div>
						    </div>
						    <div class="row">
		                		<div class="form-group">
		                			{!! Form::label('telephone_2', 'Tél. secondaire', ['class' => 'col-md-3 control-label text-right']) !!}
		                			<div class="col-md-9">
							        	{!! Form::text('telephone_2', null, ['class' => 'form-control']) !!}
						    		</div>
						    	</div>
						    </div>
						    <div class="row">
		                		<div class="form-group">
							        {!! Form::label('gender', 'Sexe', ['class' => 'col-md-3 control-label text-right']) !!}
							        <div class="col-md-9">
					        			{!! Form::select('gender', $model->getGenderTypes(), null, ['class' => 'form-control']) !!}
						    		</div>
						    	</div>
						    </div>
						    <div class="row">
		                		<div class="form-group">
							        {!! Form::label('birthdate', 'Date de naissance', ['class' => 'col-md-3 control-label text-right']) !!}
							        <div class="col-md-9">
							        	{!! Form::text('birthdate', null, ['class' => 'form-control datepicker']) !!}
							    	</div>
							    </div>
					        </div>
					        <div class="row">
		                		<div class="form-group">
							        {!! Form::label('society', 'Société', ['class' => 'col-md-3 control-label text-right']) !!}
							        <div class="col-md-9">
								        {!! Form::text('society', null, ['class' => 'form-control']) !!}
								    </div>
							    </div>
					        </div>
					        <div class="row">
		                		<div class="form-group">
							        {!! Form::label('profession', 'Profession', ['class' => 'col-md-3 control-label text-right']) !!}
							        <div class="col-md-9">
								        {!! Form::text('profession', null, ['class' => 'form-control']) !!}
								    </div>
							    </div>
					        </div>
	        			</div>

	        		</div>

	        		<div class="col-md-6">
        				<div class="header">
        					<legend class="title">Adresse</legend>
	        			</div>

	        			<div class="content">
				        	<div class="row">
		                		<div class="form-group">
		                			{!! Form::label('address_1', 'Adresse ligne 1', ['class' => 'col-md-3 control-label text-right']) !!}
		                			<div class="col-md-9">
								        {!! Form::text('address_1', null, ['class' => 'form-control']) !!}
						    		</div>
						    	</div>
						    </div>
				        	<div class="row">
		                		<div class="form-group">
		                			{!! Form::label('address_2', 'Adresse ligne 2', ['class' => 'col-md-3 control-label text-right']) !!}
		                			<div class="col-md-9">
								        {!! Form::text('address_2', null, ['class' => 'form-control']) !!}
						    		</div>
						    	</div>
						    </div>
				        	<div class="row">
		                		<div class="form-group">
		                			{!! Form::label('postcode', 'Code postal', ['class' => 'col-md-3 control-label text-right']) !!}
		                			<div class="col-md-9">
								        {!! Form::text('postcode', null, ['class' => 'form-control']) !!}
						    		</div>
						    	</div>
						    </div>
						    <div class="row">
		                		<div class="form-group">
		                			{!! Form::label('city', 'Ville', ['class' => 'col-md-3 control-label text-right']) !!}
		                			<div class="col-md-9">
								        {!! Form::text('city', null, ['class' => 'form-control']) !!}
						    		</div>
						    	</div>
						    </div>
						    <div class="row">
		                		<div class="form-group">
		                			{!! Form::label('country', 'Pays', ['class' => 'col-md-3 control-label text-right']) !!}
		                			<div class="col-md-9">
								        {!! Form::text('country', null, ['class' => 'form-control']) !!}
						    		</div>
						    	</div>
						    </div>
	        			</div>

	        		</div>

		        	<div class="clearfix"></div>
		        	{!! Form::submit('Ajouter', ['class' => 'btn btn-info btn-fill pull-right']) !!}

	        	{!! Form::close() !!}
	        </div>

	    </div>
    </div>
@endsection