@extends('admin.layout')

@section('breadcrumb')
	<a class="navbar-brand" href="{{ url('contacts') }}">Contacts</a>
	<span class="navbar-brand"> | </span>
	<a class="navbar-brand" href="{{ url('contacts/'.$model->contact_id) }}">Contact #{{ $model->contact_id }}</a>
	<span class="navbar-brand"> | </span>
	<a class="navbar-brand" href="#">Ajouter une participation</a>
@endsection

@section('content')

	<div class="col-md-12">

		<div class="card">
	        <div class="header">
	            <h4 class="title">Ajouter une participation</h4>
	        </div>
	        <div class="content">
	        	{!! Form::model($model, ['route' => ['contacts.participations.store', $model->contact_id], 'class' => 'clearfix']) !!}

	        		<div class="row">
                		<div class="form-group">
					        {!! Form::label('invited', 'Invitation envoyée', ['class' => 'col-md-3 control-label text-right']) !!}
					        <div class="col-md-9">
						        {!! Form::checkbox('invited', true, null) !!}
					        </div>
					    </div>
			        </div>
			        <div class="row">
                		<div class="form-group">
					        {!! Form::label('confirmed', 'Participation confirmée', ['class' => 'col-md-3 control-label text-right']) !!}
					        <div class="col-md-9">
						        {!! Form::checkbox('confirmed', true, null) !!}
					        </div>
					    </div>
			        </div>
					<div class="row">
        				<div class="form-group">
					        {!! Form::label('gala_id', 'événement', ['class' => 'col-md-3 control-label text-right']) !!}
        					<div class="col-md-9">
			        			{!! Form::select('gala_id', $lstGalas, null, ['class' => 'form-control']) !!}
				    		</div>
				    	</div>
        			</div>
        			<div class="row">
                		<div class="form-group">
					        {!! Form::label('invite_de', 'Invité de', ['class' => 'col-md-3 control-label text-right']) !!}
					        <div class="col-md-9">
						        {!! Form::text('invite_de', null, ['class' => 'form-control']) !!}
					        </div>
					    </div>
			        </div>
			        <div class="row">
                		<div class="form-group">
					        {!! Form::label('service_rendu', 'Service Rendu', ['class' => 'col-md-3 control-label text-right']) !!}
					        <div class="col-md-9">
						        {!! Form::text('service_rendu', null, ['class' => 'form-control']) !!}
					        </div>
					    </div>
			        </div>
			        <div class="row">
                		<div class="form-group">
					        {!! Form::label('relance', 'Relance', ['class' => 'col-md-3 control-label text-right']) !!}
					        <div class="col-md-9">
						        {!! Form::checkbox('relance', true, null) !!}
					        </div>
					    </div>
			        </div>
        			<div class="row">
        				<div class="form-group">
					        {!! Form::label('category', 'Catégorie', ['class' => 'col-md-3 control-label text-right']) !!}
        					<div class="col-md-9">
			        			{!! Form::select('category', $model->getCategoryTypes(), null, ['class' => 'form-control']) !!}
				    		</div>
				    	</div>
        			</div>
        			<div class="row">
                		<div class="form-group">
					        {!! Form::label('mot_a_joindre', 'Mot à joindre par', ['class' => 'col-md-3 control-label text-right']) !!}
					        <div class="col-md-9">
						        {!! Form::text('mot_a_joindre', null, ['class' => 'form-control']) !!}
					        </div>
					    </div>
			        </div>
			        <div class="row">
                		<div class="form-group">
					        {!! Form::label('carton_entreprise', 'Carton entreprise', ['class' => 'col-md-3 control-label text-right']) !!}
					        <div class="col-md-9">
						        {!! Form::checkbox('carton_entreprise', true, null) !!}
					        </div>
					    </div>
			        </div>
			        <div class="row">
                		<div class="form-group">
					        {!! Form::label('paiement_place', 'Paiement de place', ['class' => 'col-md-3 control-label text-right']) !!}
					        <div class="col-md-9">
						        {!! Form::checkbox('paiement_place', true, null) !!}
					        </div>
					    </div>
			        </div>
			        <div class="row">
                		<div class="form-group">
					        {!! Form::label('nb_place', 'Nombre de place(s)', ['class' => 'col-md-3 control-label text-right']) !!}
					        <div class="col-md-9">
						        {!! Form::text('nb_place', null, ['class' => 'form-control']) !!}
					        </div>
					    </div>
			        </div>
			        <div class="row">
                		<div class="form-group">
					        {!! Form::label('nb_repas', 'Nombre de dîner', ['class' => 'col-md-3 control-label text-right']) !!}
					        <div class="col-md-9">
						        {!! Form::text('nb_repas', null, ['class' => 'form-control']) !!}
					        </div>
					    </div>
			        </div>

		        	<div class="clearfix"></div>
		        	{!! Form::submit('Ajouter', ['class' => 'btn btn-info btn-fill pull-right']) !!}

	        	{!! Form::close() !!}
	        </div>

	    </div>
    </div>

@endsection
