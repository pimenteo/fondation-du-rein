@extends('admin.layout')

@section('breadcrumb')
	<a class="navbar-brand" href="{{ url('contacts') }}">Contacts</a>
	<span class="navbar-brand"> | </span>
	<a class="navbar-brand" href="#">Editer un contact</a>
	<span class="navbar-brand"> | </span>
	<a class="navbar-brand" href="#">{{ $model->full_name }}</a>
@endsection

@section('content')

	<div class="col-md-12">

		<div class="card">
	        <div class="header">
	            <h4 class="title">Modifier un contact</h4>
	        </div>
	        <div class="content contact">
	        	{!! Form::model($model, ['method' => 'put', 'route' => ['contacts.update', $model->id], 'class' => 'clearfix']) !!}  

	        		<div class="col-md-6">
	        			<div class="header">
	        				<legend class="title">Contact</legend>
		        		</div>

		        		<div class="content">
		        			<div class="row">
		        				<div class="form-group">
							        {!! Form::label('civility', 'Civilité', ['class' => 'col-md-3 control-label text-right']) !!}
		        					<div class="col-md-9">
					        			{!! Form::select('civility', $model->getCivilityTypes(), null, ['class' => 'form-control']) !!}
						    		</div>
						    	</div>
		        			</div>
		        			<div class="row free_civility">
		        				<div class="form-group">
							        {!! Form::label('free_civility', 'Autre civilité', ['class' => 'col-md-3 control-label text-right']) !!}
		        					<div class="col-md-9">
					        			{!! Form::text('free_civility', null, ['class' => 'form-control']) !!}
						    		</div>
						    	</div>
		        			</div>
		        			<div class="row">
		                		<div class="form-group">
							        {!! Form::label('particule', 'Particule', ['class' => 'col-md-3 control-label text-right']) !!}
							        <div class="col-md-9">
								        {!! Form::text('particule', null, ['class' => 'form-control']) !!}
							        </div>
							    </div>
					        </div>
					        <div class="row">
		                		<div class="form-group">
							        {!! Form::label('firstname', 'Nom', ['class' => 'col-md-3 control-label text-right']) !!}
							        <div class="col-md-9">
								        {!! Form::text('firstname', null, ['class' => 'form-control']) !!}
							        </div>
							    </div>
					        </div>
					        <div class="row">
		                		<div class="form-group">
							        {!! Form::label('lastname', 'Prénom', ['class' => 'col-md-3 control-label text-right']) !!}
							        <div class="col-md-9">
								        {!! Form::text('lastname', null, ['class' => 'form-control']) !!}
							        </div>
							    </div>
					        </div>
					        <div class="row">
		                		<div class="form-group">
							        {!! Form::label('email', 'Adresse E-mail', ['class' => 'col-md-3 control-label text-right']) !!}
							        <div class="col-md-9">
							        	{!! Form::text('email', null, ['class' => 'form-control']) !!}
							    	</div>
							    </div>
					        </div>
					        <div class="row">
		                		<div class="form-group">
		                			{!! Form::label('telephone_1', 'Tél. principal', ['class' => 'col-md-3 control-label text-right']) !!}
		                			<div class="col-md-9">
							        	{!! Form::text('telephone_1', null, ['class' => 'form-control']) !!}
						    		</div>
						    	</div>
						    </div>
						    <div class="row">
		                		<div class="form-group">
		                			{!! Form::label('telephone_2', 'Tél. secondaire', ['class' => 'col-md-3 control-label text-right']) !!}
		                			<div class="col-md-9">
							        	{!! Form::text('telephone_2', null, ['class' => 'form-control']) !!}
						    		</div>
						    	</div>
						    </div>
						    <div class="row">
		                		<div class="form-group">
							        {!! Form::label('gender', 'Sexe', ['class' => 'col-md-3 control-label text-right']) !!}
							        <div class="col-md-9">
					        			{!! Form::select('gender', $model->getGenderTypes(), null, ['class' => 'form-control']) !!}
						    		</div>
						    	</div>
						    </div>
						    <div class="row">
		                		<div class="form-group">
							        {!! Form::label('birthdate', 'Date de naissance', ['class' => 'col-md-3 control-label text-right']) !!}
							        <div class="col-md-9">
							        	{!! Form::text('birthdate', $model->birthdate ? $model->birthdate->format('d/m/Y') : '', ['class' => 'form-control datepicker']) !!}
							    	</div>
							    </div>
					        </div>
					        <div class="row">
		                		<div class="form-group">
							        {!! Form::label('society', 'Société', ['class' => 'col-md-3 control-label text-right']) !!}
							        <div class="col-md-9">
								        {!! Form::text('society', null, ['class' => 'form-control']) !!}
								    </div>
							    </div>
					        </div>
					        <div class="row">
		                		<div class="form-group">
							        {!! Form::label('profession', 'Profession', ['class' => 'col-md-3 control-label text-right']) !!}
							        <div class="col-md-9">
								        {!! Form::text('profession', null, ['class' => 'form-control']) !!}
								    </div>
							    </div>
					        </div>
	        			</div>

	        		</div>

	        		<div class="col-md-6">
        				<div class="header">
        					<legend class="title">Adresse</legend>
	        			</div>

	        			<div class="content">
				        	<div class="row">
		                		<div class="form-group">
		                			{!! Form::label('address_1', 'Adresse ligne 1', ['class' => 'col-md-3 control-label text-right']) !!}
		                			<div class="col-md-9">
								        {!! Form::text('address_1', null, ['class' => 'form-control']) !!}
						    		</div>
						    	</div>
						    </div>
				        	<div class="row">
		                		<div class="form-group">
		                			{!! Form::label('address_2', 'Adresse ligne 2', ['class' => 'col-md-3 control-label text-right']) !!}
		                			<div class="col-md-9">
								        {!! Form::text('address_2', null, ['class' => 'form-control']) !!}
						    		</div>
						    	</div>
						    </div>
				        	<div class="row">
		                		<div class="form-group">
		                			{!! Form::label('postcode', 'Code postal', ['class' => 'col-md-3 control-label text-right']) !!}
		                			<div class="col-md-9">
								        {!! Form::text('postcode', null, ['class' => 'form-control']) !!}
						    		</div>
						    	</div>
						    </div>
						    <div class="row">
		                		<div class="form-group">
		                			{!! Form::label('city', 'Ville', ['class' => 'col-md-3 control-label text-right']) !!}
		                			<div class="col-md-9">
								        {!! Form::text('city', null, ['class' => 'form-control']) !!}
						    		</div>
						    	</div>
						    </div>
						    <div class="row">
		                		<div class="form-group">
		                			{!! Form::label('country', 'Pays', ['class' => 'col-md-3 control-label text-right']) !!}
		                			<div class="col-md-9">
								        {!! Form::text('country', null, ['class' => 'form-control']) !!}
						    		</div>
						    	</div>
						    </div>
	        			</div>

	        		</div>

		        	<div class="clearfix"></div>
		        	{!! Form::submit('Enregistrer', ['class' => 'btn btn-info btn-fill pull-right']) !!}

	        	{!! Form::close() !!}
	        </div>
	    </div>

	    <div class="card">
	    	<div class="header">
                <h4 class="title">Présent(e) aux événements suivants</h4>
                <div class="clearfix">
                	<a href="{{ route('contacts.participations.create', $model->id) }}" class="btn btn-success btn-fill">Ajouter une participation</a>
                </div>
            </div>
            <div class="content table-responsive table-full-width">

            	<table class="table table-hover table-striped table-hover table-condensed">
            		<thead>
                        <tr>
	                    	<th>Désignation</th>
	                    	<th>Date</th>
	                    	<th class="actions">Actions</th>
	                    </tr>
	                </thead>
	                @if (!empty($model->participations))
	                    <tbody>
	                    	@foreach ($model->participations as $participation)
		                    	<tr>
		                    		<td>{{ $participation->gala->name }}</td>
		                    		<td>{{ $participation->gala->date ? $participation->gala->date->format('d/m/Y') : '' }}</td>
		                    		<td class="actions">
		                    			<a href="{{ route('contacts.participations.show', [$participation->contact_id, $participation->id]) }}" class="btn btn-xs btn-info btn-fill pull-right">
		                        			Voir
		                        		</a>
		                        		{!! Form::open(['method' => 'delete', 'route' => ['contacts.participations.destroy', $participation->contact_id, $participation->id], 'class' => 'pull-right', 'onsubmit' => 'return confirm(\'Supprimer définitivement la ligne ?\')']) !!}
		                        			{!! Form::submit('Supprimer', ['class' => 'btn btn-xs btn-danger btn-fill']) !!}
		                        		{!! Form::close() !!}
		                    		</td>
		                    	</tr>
	                    	@endforeach
	                    </tbody>
                    @endif
            	</table>

    		</div>
    	</div>

    	<div class="card">
	    	<div class="header">
                <h4 class="title">Dons <em>(Total : {{ $model->total_dons }}€)</em></h4>
                <div class="clearfix">
                	<a href="{{ route('contacts.dons.create', $model->id) }}" class="btn btn-success btn-fill">Ajouter un don</a>
                </div>
            </div>
            <div class="content table-responsive table-full-width">

            	<table class="table table-hover table-striped table-hover table-condensed">
            		<thead>
                        <tr>
	                    	<th>Montant</th>
	                    	<th>Date</th>
	                    	<th>Moyen de paiement</th>
	                    	<th class="actions">Actions</th>
	                    </tr>
	                </thead>
	                @if (!empty($model->dons))
	                    <tbody>
	                    	@foreach ($model->dons as $don)
		                    	<tr>
		                    		<td>{{ $don->amount }} €</td>
		                    		<td>{{ $don->formatted_date_don }}</td>
		                    		<td>{{ $don->payment }}</td>
		                    		<td class="actions">
		                    			{!! Form::open(['method' => 'delete', 'route' => ['contacts.dons.destroy', $don->contact_id, $don->id], 'class' => 'pull-right', 'onsubmit' => 'return confirm(\'Supprimer définitivement la ligne ?\')']) !!}
		                        			{!! Form::submit('Supprimer', ['class' => 'btn btn-xs btn-danger btn-fill']) !!}
		                        		{!! Form::close() !!}
		                    		</td>
		                    	</tr>
	                    	@endforeach
	                    </tbody>
                    @endif
            	</table>

    		</div>
    	</div>

    </div>

@endsection