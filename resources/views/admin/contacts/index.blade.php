@extends('admin.layout')

@section('breadcrumb')
	<a class="navbar-brand" href="#">Contacts</a>
@endsection

@section('searchform')
	{!! $searchForm or '' !!}
@endsection

@section('content')
	
	<div class="col-md-12">
        
        @yield('searchform')
        
        <div class="card">
            <div class="header">
                <h4 class="title">Liste des contacts</h4>
                <div class="clearfix">
                	<a href="{{ url('contacts/create') }}" class="btn btn-success btn-fill">Ajouter un contact</a>
                </div>
            </div>

            <div class="content table-responsive table-full-width">
                <table class="table table-hover table-striped table-hover table-condensed">
                    <thead>
                        <tr>
	                    	<th>Nom</th>
	                    	<th>Prénom</th>
	                    	<th>Code postal</th>
	                    	<th>Ville</th>
	                    	<th>Invité de</th>
	                    	<th>Invitation envoyée</th>
	                    	<th>Participation confirmée</th>
	                    	<th class="actions">Actions</th>
	                    </tr>
	                </thead>
	                @if (!empty($datas))
	                    <tbody>

	                    	@foreach ($datas as $contact)
		                        <tr>
		                        	<td>{{ $contact->firstname }}</td>
		                        	<td>{{ $contact->lastname }}</td>
		                        	<td>{{ $contact->postcode }}</td>
		                        	<td>{{ $contact->city }}</td>
		                        	<td>{{ $contact->invite_de }}</td>
		                        	<td>{!! $contact->invited_btn !!}</td>
		                        	<td>{!! $contact->confirmed_btn !!}</td>
		                        	<td class="actions">
		                        		<a href="{{ url('contacts/'.$contact->id) }}" class="btn btn-xs btn-info btn-fill pull-right">
		                        			Voir
		                        		</a>
		                        		{!! Form::open(['method' => 'delete', 'route' => ['contacts.destroy', $contact->id], 'class' => 'pull-right', 'onsubmit' => 'return confirm(\'Supprimer définitivement la ligne ?\')']) !!}
		                        			{!! Form::submit('Supprimer', ['class' => 'btn btn-xs btn-danger btn-fill']) !!}
		                        		{!! Form::close() !!}
		                        	</td>
		                        </tr>
	                        @endforeach
	                    </tbody>
					@endif
                </table>

                @if ($query->lastPage() > 1)
                <ul class="pagination pull-right">
				    <li class="{{ ($query->currentPage() == 1) ? ' disabled' : '' }}">
				        <a href="{{ $query->url(1) }}"><i class="fa fa-chevron-left"></i></a>
				    </li>
	                @for ($i = $initial = max(1, $query->currentPage() - 5), $l = min($initial + 10, $query->lastPage()); $i <= $l; $i++)
	                	<li class="{{ ($query->currentPage() == $i) ? ' active' : '' }}">
				            <a href="{{ $query->url($i) }}">{{ $i }}</a>
				        </li>
					@endfor
					<li class="{{ ($query->currentPage() == $query->lastPage()) ? ' disabled' : '' }}">
				        <a href="{{ $query->url($query->currentPage()+1) }}" ><i class="fa fa-chevron-right"></i></a>
				    </li>
				</ul>
				@endif

            </div>
        </div>
    </div>

@endsection