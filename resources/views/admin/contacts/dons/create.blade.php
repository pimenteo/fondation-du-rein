@extends('admin.layout')

@section('breadcrumb')
	<a class="navbar-brand" href="{{ url('contacts') }}">Contacts</a>
	<span class="navbar-brand"> | </span>
	<a class="navbar-brand" href="{{ url('contacts/'.$model->contact_id) }}">Contact #{{ $model->contact_id }}</a>
	<span class="navbar-brand"> | </span>
	<a class="navbar-brand" href="#">Ajouter un don</a>
@endsection

@section('content')

	<div class="col-md-12">

		<div class="card">
	        <div class="header">
	            <h4 class="title">Ajouter un don</h4>
	        </div>
	        <div class="content">
	        	{!! Form::model($model, ['route' => ['contacts.dons.store', $model->contact_id], 'class' => 'clearfix']) !!}
	        		
			        <div class="row">
                		<div class="form-group">
					        {!! Form::label('amount', 'Montant', ['class' => 'col-md-3 control-label text-right']) !!}
					        <div class="col-md-9">
						        {!! Form::text('amount', null, ['class' => 'form-control']) !!}
					        </div>
					    </div>
			        </div>
			        <div class="row">
                		<div class="form-group">
					        {!! Form::label('date_don', 'Date du don', ['class' => 'col-md-3 control-label text-right']) !!}
					        <div class="col-md-9">
						        {!! Form::text('date_don', null, ['class' => 'form-control datepicker']) !!}
					        </div>
					    </div>
			        </div>
			        <div class="row">
        				<div class="form-group">
					        {!! Form::label('payment', 'Moyen de paiement', ['class' => 'col-md-3 control-label text-right']) !!}
        					<div class="col-md-9">
			        			{!! Form::select('payment', $model->getPaymentTypes(), null, ['class' => 'form-control']) !!}
				    		</div>
				    	</div>
        			</div>
        			<div class="row">
        				<div class="form-group">
					        {!! Form::label('marketing', 'Moyen marketing', ['class' => 'col-md-3 control-label text-right']) !!}
        					<div class="col-md-9">
			        			{!! Form::select('marketing', $model->getMarketingTypes(), null, ['class' => 'form-control']) !!}
				    		</div>
				    	</div>
        			</div>

		        	<div class="clearfix"></div>
		        	{!! Form::submit('Ajouter', ['class' => 'btn btn-info btn-fill pull-right']) !!}

	        	{!! Form::close() !!}
	        </div>

	    </div>
    </div>

@endsection