@extends('admin.layout')

@section('breadcrumb')
	<a class="navbar-brand" href="{{ url('administrators') }}">Administrateurs</a>
	<span class="navbar-brand"> | </span>
	<a class="navbar-brand" href="#">Ajouter un administrateur</a>
@endsection

@section('content')

	<div class="col-md-12">
		<div class="card">
	        <div class="header">
	            <h4 class="title">Ajouter un administrateur</h4>
	        </div>
	        <div class="content">
	        	{!! Form::model($model, ['route' => 'administrators.store', 'class' => 'clearfix']) !!}  

        			<div class="row">
        				<div class="col-md-6">
	                		<div class="form-group row">
						        {!! Form::label('name', 'Nom', ['class' => 'col-md-3 control-label text-right']) !!}
						        <div class="col-md-9">
							        {!! Form::text('name', null, ['class' => 'form-control']) !!}
						        </div>
						    </div>
			        	</div>
			        	<div class="col-md-6">
	                		<div class="form-group row">
						        {!! Form::label('email', 'E-mail', ['class' => 'col-md-3 control-label text-right']) !!}
						        <div class="col-md-9">
							        {!! Form::email('email', null, ['class' => 'form-control']) !!}
						        </div>
						    </div>
						</div>
						<div class="col-md-6">
	                		<div class="form-group row">
						        {!! Form::label('password', 'Mot de passe', ['class' => 'col-md-3 control-label text-right']) !!}
						        <div class="col-md-9">
							        {!! Form::password('password', ['class' => 'form-control']) !!}
						        </div>
						    </div>
			        	</div>
			        	<div class="col-md-6">
	                		<div class="form-group row">
						        {!! Form::label('password_confirm', 'Confirmation du mot de passe', ['class' => 'col-md-3 control-label text-right']) !!}
						        <div class="col-md-9">
							        {!! Form::password('password_confirm', ['class' => 'form-control']) !!}
						        </div>
						    </div>
						</div>
			        </div>

		        	<div class="clearfix"></div>
		        	{!! Form::submit('Ajouter', ['class' => 'btn btn-info btn-fill pull-right']) !!}

	        	{!! Form::close() !!}
	        </div>

	    </div>
    </div>

@endsection

