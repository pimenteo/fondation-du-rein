@extends('admin.layout')

@section('breadcrumb')
	<a class="navbar-brand" href="#">Administrateurs</a>
@endsection

@section('content')
	
	<div class="col-md-12">
        <div class="card">
            <div class="header">
                <h4 class="title">Liste des administrateurs</h4>
                <div class="clearfix">
                	<a href="{{ url('administrators/create') }}" class="btn btn-success btn-fill">Ajouter un administrateur</a>
                </div>
            </div>
            <div class="content table-responsive table-full-width">

                <table class="table table-hover table-striped">
                    <thead>
                        <tr>
	                    	<th>Nom</th>
	                    	<th>E-mail</th>
	                    	<th>Date de création</th>
	                    	<th class="actions">Actions</th>
	                    </tr>
	                </thead>
	                @if (!empty($datas))
	                    <tbody>
	                    	@foreach ($datas as $user)
		                        <tr>
		                        	<td>{{ $user->name }}</td>
		                        	<td><a href="mailto:{{ $user->email }}">{{ $user->email }}</a></td>
		                        	<td>{{ $user->created_at->format('d/m/Y') }}</td>
		                        	<td class="actions">
		                        		<a href="{{ url('administrators/'.$user->id) }}" class="btn btn-xs btn-info btn-fill pull-right">
		                        			Voir
		                        		</a>
		                        		{!! Form::open(['method' => 'delete', 'route' => ['administrators.destroy', $user->id], 'class' => 'pull-right', 'onsubmit' => 'return confirm(\'Supprimer définitivement la ligne ?\')']) !!}
		                        			{!! Form::submit('Supprimer', ['class' => 'btn btn-xs btn-danger btn-fill']) !!}
		                        		{!! Form::close() !!}
		                        	</td>
		                        </tr>
	                        @endforeach
	                    </tbody>
					@endif
                </table>

            </div>
        </div>
    </div>

@endsection