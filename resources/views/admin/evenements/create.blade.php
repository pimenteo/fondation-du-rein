@extends('admin.layout')

@section('breadcrumb')
	<a class="navbar-brand" href="{{ url('evenements') }}">Evénements</a>
	<span class="navbar-brand"> | </span>
	<a class="navbar-brand" href="#">Ajouter un événement</a>
@endsection

@section('content')

	<div class="col-md-12">
		<div class="card">
	        <div class="header">
	            <h4 class="title">Ajouter un événement</h4>
	        </div>
	        <div class="content">
	        	{!! Form::model($model, ['route' => 'evenements.store', 'class' => 'clearfix']) !!}  

        			<div class="row">
        				<div class="col-md-6">
	                		<div class="form-group">
						        {!! Form::label('name', 'Nom', ['class' => 'col-md-3 control-label text-right']) !!}
						        <div class="col-md-9">
							        {!! Form::text('name', null, ['class' => 'form-control']) !!}
						        </div>
						    </div>
				        </div>
			        	<div class="col-md-6">
	                		<div class="form-group">
						        {!! Form::label('date', 'Date', ['class' => 'col-md-3 control-label text-right']) !!}
						        <div class="col-md-9">
							        {!! Form::text('date', null, ['class' => 'form-control datepicker']) !!}
						        </div>
						    </div>
				        </div>
				    </div>
		        	<div class="clearfix"></div>
		        	{!! Form::submit('Ajouter', ['class' => 'btn btn-info btn-fill pull-right']) !!}

	        	{!! Form::close() !!}
	        </div>

	    </div>
    </div>

@endsection

