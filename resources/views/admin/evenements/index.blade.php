@extends('admin.layout')

@section('breadcrumb')
	<a class="navbar-brand" href="#">Evénements</a>
@endsection

@section('content')
	
	<div class="col-md-12">
        <div class="card">
            <div class="header">
                <h4 class="title">Liste des événements</h4>
                <div class="clearfix">
                	<a href="{{ url('evenements/create') }}" class="btn btn-success btn-fill">Ajouter un événement</a>
                </div>
            </div>
            <div class="content table-responsive table-full-width">

                <table class="table table-hover table-striped">
                    <thead>
                        <tr>
	                    	<th>Nom</th>
	                    	<th>Date</th>
	                    	<th class="actions">Actions</th>
	                    </tr>
	                </thead>
	                @if (!empty($datas))
	                    <tbody>
	                    	@foreach ($datas as $gala)
		                        <tr>
		                        	<td>{{ $gala->name }}</td>
		                        	<td>{{ $gala->date->format('d/m/Y') }}</td>
		                        	<td class="actions">
		                        		@if ($gala->getNotInvited(true) > 0)
		                        			<a href="{{ url('evenements/invite/'.$gala->id) }}" class="btn btn-xs btn-success btn-fill">
		                        				Générer les participations
	                        				</a>
		                        		@endif
		                        		<a href="{{ url('evenements/'.$gala->id) }}" class="btn btn-xs btn-info btn-fill">
		                        			Voir
		                        		</a>
		                        	</td>
		                        </tr>
	                        @endforeach
	                    </tbody>
					@endif
                </table>

                @if ($query->lastPage() > 1)
                <ul class="pagination pull-right">
				    <li class="{{ ($query->currentPage() == 1) ? ' disabled' : '' }}">
				        <a href="{{ $query->url(1) }}"><i class="fa fa-chevron-left"></i></a>
				    </li>
	                @for ($i = $initial = max(1, $query->currentPage() - 5), $l = min($initial + 10, $query->lastPage()); $i <= $l; $i++)
	                	<li class="{{ ($query->currentPage() == $i) ? ' active' : '' }}">
				            <a href="{{ $query->url($i) }}">{{ $i }}</a>
				        </li>
					@endfor
					<li class="{{ ($query->currentPage() == $query->lastPage()) ? ' disabled' : '' }}">
				        <a href="{{ $query->url($query->currentPage()+1) }}" ><i class="fa fa-chevron-right"></i></a>
				    </li>
				</ul>
				@endif
				
            </div>
        </div>
    </div>

@endsection