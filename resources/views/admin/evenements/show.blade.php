@extends('admin.layout')

@section('breadcrumb')
	<a class="navbar-brand" href="{{ url('evenements') }}">Evénements</a>
	<span class="navbar-brand"> | </span>
	<a class="navbar-brand" href="#">Editer un événement</a>
	<span class="navbar-brand"> | </span>
	<a class="navbar-brand" href="#">{{ $model->name }}</a>
@endsection

@section('content')

	<div class="col-md-12">
		<div class="card">
	        <div class="header">
	            <h4 class="title">Modifier un événement</h4>
	        </div>
	        <div class="content">
	        	{!! Form::model($model, ['method' => 'put', 'route' => ['evenements.update', $model->id], 'class' => 'clearfix']) !!}  

        			<div class="row">
                		<div class="form-group">
					        {!! Form::label('name', 'Nom', ['class' => 'col-md-3 control-label text-right']) !!}
					        <div class="col-md-9">
						        {!! Form::text('name', null, ['class' => 'form-control']) !!}
					        </div>
					    </div>
			        </div>
			        <div class="row">
                		<div class="form-group">
					        {!! Form::label('date', 'Date', ['class' => 'col-md-3 control-label text-right']) !!}
					        <div class="col-md-9">
						        {!! Form::text('date', $model->date->format('d/m/Y'), ['class' => 'form-control datepicker']) !!}
					        </div>
					    </div>
			        </div>

		        	<div class="clearfix"></div>
		        	{!! Form::submit('Enregistrer', ['class' => 'btn btn-info btn-fill pull-right']) !!}

	        	{!! Form::close() !!}
	        </div>

	    </div>
    </div>

@endsection

