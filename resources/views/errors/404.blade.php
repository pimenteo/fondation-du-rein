@extends('layout')

@section('css')
    <link rel="stylesheet" href="{{ url('front/css/errors.css') }}">
@endsection

@section('content')

	<div id="message_bag" class="panel panel-default">
		<h1 class="panel-heading">Page introuvable</h1>
        <div class="panel-body">
			La page que vous cherchez n'existe pas
		</div>
	</div>

@endsection
