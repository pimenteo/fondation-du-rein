<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

        <script src="{{ url('front/js/jquery.min.js') }}"></script>
        <script src="{{ url('front/js/swiper.min.js') }}"></script>
        <script src="{{ url('front/js/fancybox/jquery.fancybox.js') }}"></script>

        <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="{{ url('front/css/global.css') }}">
        <link rel="stylesheet" href="{{ url('front/css/font-awesome.min.css') }}">
        <link rel="stylesheet" href="{{ url('front/css/swiper.min.css') }}">
        <link rel="stylesheet" href="{{ url('front/js/fancybox/jquery.fancybox.css') }}">

        @yield('css')
    </head>

    <body class="container-fluid">

        @include('helpers.messagebag')

        <div class="wrapper row">
            <div class="background-motif background-header"></div>

            <div class="body-container container">
                @yield('content')
            </div>

            <!-- <div class="background-motif background-footer"></div> -->
        </div>
    </body>
</html>
