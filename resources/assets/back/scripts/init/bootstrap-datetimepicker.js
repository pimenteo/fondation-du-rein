
// Use fontawesome icons
$.fn.datetimepicker.defaults.icons = {
    time: 'fa fa-clock-o',
    date: 'fa fa-calendar-o',
    up: 'fa fa-chevron-up',
    down: 'fa fa-chevron-down',
    previous: 'fa fa-chevron-left',
    next: 'fa fa-chevron-right',
    today: 'fa fa-crosshairs',
    clear: 'fa fa-trash',
    close: 'fa fa-times',
}

// Set locale
$.fn.datetimepicker.defaults.locale = moment.locale()

// Disable auto current date
$.fn.datetimepicker.defaults.useCurrent = false
