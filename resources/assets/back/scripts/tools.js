
$(document).ready(function() {
	
	//*** Displaying search form *******//
	// $('#searchBtn').click(function() {
	// 	$('#sao-container').slideToggle('slow');
	// 	return false;
	// });
	//*** END Displaying search form ***//


	//*** Displaying datepicker ********//
	$('.datepicker').datetimepicker({
		locale: moment.locale(),
		// debug: true,
		format: 'DD/MM/YYYY'
	})
	//*** END Displaying datepicker ****//

	displayFreeCivility();
})


$(document).on("change", ".contact #civility", function() {
	displayFreeCivility();
});

function displayFreeCivility()
{
	if($(".contact #civility").val() != "") {
		$(".contact .free_civility").slideUp('slow');
	} else {
		$(".contact .free_civility").slideDown('slow');
	}
}

$(document).on("click", ".pagination li a", function(e) {
	e.preventDefault();

	document.location.href = $(this).attr("href")+"&"+$("#searchForm").closest('form').serialize()
	console.log($("#searchForm").closest('form').serialize());
});