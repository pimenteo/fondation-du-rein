
var path = require('path');
var webpack = require('webpack');

module.exports = [{
    resolve: {
        root: [
            path.resolve(__dirname + '/resources/assets/back'),
        ],
    },
    entry: [
        'bootstrap-sass/assets/javascripts/bootstrap.min.js',
        // 'scripts/plugins/bootstrap-checkbox-radio-switch.js',
        'scripts/plugins/bootstrap-datetimepicker.min.js',
        // 'scripts/plugins/bootstrap-notify.js',
        // 'scripts/plugins/bootstrap-select.js',
        // 'scripts/plugins/chartist.min.js',
        'scripts/plugins/light-bootstrap-dashboard.js',
        'scripts/init/bootstrap-datetimepicker.js',
        'scripts/init/moment.js',
        'scripts/tools.js',
    ],
    output: {
        path: path.join(__dirname, 'public', 'back'),
        filename: 'private.built.js',
        publicPath: '/back/',
    },
    module: {
        loaders: [],
    },
    plugins: [
        new webpack.ProvidePlugin({
            $: 'jquery',
            'jQuery': 'jquery',
            'window.jQuery': 'jquery',
            intl: 'intl',
            moment: 'moment',
        }),
    ],
}, {
    resolve: {
        root: [
            path.resolve(__dirname + '/resources/assets/back'),
        ],
    },
    entry: [
        'styles/bootstrap.scss',
        'styles/app.css',
        // 'styles/plugins/animate.min.css',
        'styles/plugins/light-bootstrap-dashboard.css',
        'styles/plugins/bootstrap-datetimepicker.min.css',
        // 'styles/plugins/pe-icon-7-stroke.css',
        'font-awesome/scss/font-awesome.scss',
        // 'select2/dist/css/select2.min.css',
        // 'sweetalert/dist/sweetalert.css',
        // 'select2-bootstrap-theme/dist/select2-bootstrap.min.css',
        // 'tinymce/skins/lightgray/content.min.css',
        // 'tinymce/skins/lightgray/skin.min.css',
    ],
    watchOptions: {
        poll: true
    },
    output: {
        path: path.join(__dirname, 'public', 'back'),
        filename: 'public.built.js',
        publicPath: '//fondation-du-rein.dev/',
    },
    module: {
        loaders: [
            {
                test: /\.(png|jpg|gif)$/,
                loader: 'file-loader',
                query: {
                    name: '[name].[ext]',
                },
            }, {
                test: /\.css$/,
                loaders: ['style', 'css'],
            }, {
                test: /\.scss$/,
                loaders: ['style', 'css', 'sass'],
            }, {
                test: /\.(woff|woff2|ttf|eot|svg)(\?\S*)?$/,
                loader: 'file-loader',
                query: {
                    name: '[name].[ext]',
                },
            },
        ],
      },
    plugins: []
}]
