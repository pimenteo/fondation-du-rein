
var path = require('path')
var webpack = require('webpack')
var config = require(path.join(__dirname, 'webpack-common'))

for (var i = 0; i < config.length; ++i) {

    config[i].plugins.unshift(new webpack.DefinePlugin({
        'process.env': {
            'NODE_ENV': JSON.stringify('production'),
        },
    }))

    config[i].plugins.push(new webpack.optimize.DedupePlugin())

    config[i].plugins.push(new webpack.optimize.UglifyJsPlugin({
        minimize: true,
        compress: {
            warnings: false,
        },
    }))

    config[i].plugins.push(new webpack.optimize.OccurrenceOrderPlugin())

    config[i].output.publicPath = '/back/'
}

module.exports = config
