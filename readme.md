# Laravel base

Base de projet utilisant Laravel 5.1 LTS


## Fork et installation du projet

``` composer install && npm install && cp .env.example .env && php artisan key:generate && php artisan migrate:install && php artisan migrate:refresh --seed```

## Intégration en continue (sync parent repository)

-- après le clone du fork projet
``` git remote add upstream https://github.com/ORIGINAL_OWNER/ORIGINAL_REPOSITORY.git ```

-- sync the fork

Fetch the branches and their respective commits from the upstream repository. Commits to master will be stored in a local branch, upstream/master.
``` git fetch upstream ```

Check out your fork's local master branch.
``` git checkout master ```

Merge the changes from upstream/master into your local master branch. This brings your fork's master branch into sync with the upstream repository, without losing your local changes.
``` git merge upstream/master ```

If your local branch didn't have any unique commits, Git will instead perform a "fast-forward":
``` git git merge upstream/master ```


## Webpack dev server

Lancez `webpack-dev-server --config webpack-dev.config.js --inline --hot -w` puis ouvrir `http://resipelec-erp.dev:3000/webpack-dev-server/`




## Laravel PHP Framework

[![Build Status](https://travis-ci.org/laravel/framework.svg)](https://travis-ci.org/laravel/framework)
[![Total Downloads](https://poser.pugx.org/laravel/framework/d/total.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/framework/v/stable.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/framework/v/unstable.svg)](https://packagist.org/packages/laravel/framework)
[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as authentication, routing, sessions, queueing, and caching.

Laravel is accessible, yet powerful, providing powerful tools needed for large, robust applications. A superb inversion of control container, expressive migration system, and tightly integrated unit testing support give you the tools you need to build any application with which you are tasked.

## Official Documentation

Documentation for the framework can be found on the [Laravel website](http://laravel.com/docs).

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](http://laravel.com/docs/contributions).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell at taylor@laravel.com. All security vulnerabilities will be promptly addressed.

### License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)
