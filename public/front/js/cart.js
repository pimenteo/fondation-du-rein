$(document).ready(function() {
    cart = {
        products: [],
        total_amount: 0,
        init: function() {
            var total_amount = 0;
            $("tr.product").each(function() {
                var product = {};
                product.id = $(this).find(".id_product").val();
                product.quantity = $(this).find(".cart_quantity").val();
                product.unit_price = $(this).find(".unit_price").val();
                product.unit_tax = $(this).find(".unit_tax").val();
                product.total_ttc = $(this).find(".total_ttc").val();
                cart.products.push(product);

                total_amount += parseFloat($(this).find(".total_ttc").val());
            });

            cart.total_amount = total_amount;
        },
        updateCartCookie: function(product_id, quantity, remove) {
            $.ajax({
                type: 'GET',
                url: base_url+'/ajax/cart/',
                data: {
                    product_id: product_id,
                    new_quantity: quantity,
                    remove: remove
                },
                dataType: 'json',
                success: function(data) {
                    if(typeof data.error != 'undefined') {
                        alert(data.error);
                    }
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    console.warn(errorThrown);
                }
            });
        },
        updateQuantity: function(id_product) {
            $.each(cart.products, function( key, product ) {
                if(product.id == id_product) {

                    var current_product = $("tr.product[data-id='"+id_product+"']");

                    $.ajax({
                        type: 'GET',
                        url: base_url+'/ajax/products/'+id_product,
                        dataType: 'json',
                        success: function(data) {
                            if(parseInt(current_product.find('.cart_quantity').val()) <= data.quantity && parseInt(current_product.find('.cart_quantity').val()) != product.quantity)
                            {
                                product.quantity = parseInt(current_product.find('.cart_quantity').val());
                                product.total_ttc = (parseInt(product.quantity) * parseFloat(product.unit_price)).toFixed(2);

                                $("tr.product[data-id='"+id_product+"']").find('.total_ttc').val(product.total_ttc);
                                $("tr.product[data-id='"+id_product+"']").find('.product_total_ttc span').html(parseFloat(product.total_ttc).formatMoney(2));

                                cart.updateCartCookie(product.id, product.quantity, 0);
                                cart.updateTotalAmount();
                                cart.updateCountProducts();
                            }
                            else
                            {
                                current_product.find('.cart_quantity').val(data.quantity);
                                alert("La quantité maximale de ce produit a été atteinte");
                            }
                        },
                        error: function(XMLHttpRequest, textStatus, errorThrown) {
                            console.warn(errorThrown);
                        }
                    });
                }
            });
        },
        updateCountProducts: function() {
            var total_quantity = 0;
            $.each(cart.products, function( key, product ) {
                total_quantity += parseInt(product.quantity);
            });
            $(".cart_table .count_products .count").html(total_quantity);
        },
        updateTotalAmount: function() {
            var total_amount = 0;
            $.each(cart.products, function( key, product ) {
                total_amount += product.quantity * (parseFloat(product.unit_price) + parseFloat(product.unit_tax));
            });
            cart.products.total_amount = total_amount;

            $(".cart_table .cart_total_ttc").html(parseFloat(total_amount).formatMoney(2));
            $(".header .cart .cart_price").html(parseFloat(total_amount).formatMoney(2));
        },
        removeProduct: function(id_product) {

            var key_to_delete = -1;
            $.each(cart.products, function( key, product ) {
                if(product.id == id_product) {
                    key_to_delete = key;
                    $("tr.product[data-id='"+id_product+"']").remove();

                    cart.updateCartCookie(product.id, product.quantity, 1);
                }
            });

            // Suppression du tableau
            if(key_to_delete > -1) {
                cart.products.splice(key_to_delete, 1);
            }

            if(cart.products.length <= 0) {
                $(".cart_table").replaceWith('<p>Votre panier est vide</p>');
            }
            else {
                cart.updateCountProducts();
            }
        },
    };

    cart.init();
});

$(document).on("click", ".quantity .buttons .plus", function(){
    var current_product = $(this).closest("tr.product");
    var current_quantity = current_product.find(".quantity .cart_quantity");

    current_quantity.val(parseInt(current_quantity.val()) + 1);
    cart.updateQuantity(current_product.find('.id_product').val());
});

$(document).on("click", ".quantity .buttons .moins", function(){
    var current_product = $(this).closest("tr.product");
    var current_quantity = current_product.find(".quantity .cart_quantity");

    if(parseInt(current_quantity.val()) > 1) {
        current_quantity.val(parseInt(current_quantity.val()) - 1);
        cart.updateQuantity(current_product.find('.id_product').val());
    }
});

$(document).on("focusout", ".cart_quantity", function() {
    var current_product = $(this).closest("tr.product");
    var current_quantity = current_product.find(".quantity .cart_quantity");

    if(parseInt(current_quantity.val()) > 0) {
        cart.updateQuantity(current_product.find('.id_product').val());
    }
    else {
        cart.removeProduct(current_product.find('.id_product').val());
    }
});

$(document).on("click", ".delete", function() {
    var current_product = $(this).closest("tr.product");
    cart.removeProduct(current_product.find('.id_product').val());

    return false;
});
