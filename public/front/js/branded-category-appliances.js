$(document).on("keyup", "#appliance_search", debounce(250, function(){
    product_list.requestAppliancesLexical();
}));

$(document).on("click", "#results_search .letter .elements li a", function(){

    var appliance_id = $(this).data('id');

    $('input[name="appliance_id"]').val(appliance_id);

    product_list.values['page'] = 1;
    product_list.values['produits'] = appliance_id;
    product_list.values['recherche'] = $('#appliance_search').val();
    product_list.request();

    return false;
});



$(document).on("click", ".navigation .back", function(){
    $("#products-list").hide();
    $("#search-panel").show();

    $('html, body').animate({
        scrollTop: $("#ajax-panel").offset().top
    }, 1000);

    $(this).parent().remove();

    product_list.values['produits'] = null;
    product_list.values['page'] = null;

    product_list.refreshHash();

    return false;
});



