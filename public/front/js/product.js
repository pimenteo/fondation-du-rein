$(document).ready(function() {

    context = {

        product: {
            id: $('input[name="product_id"]').val(),
            price_tax_excl: parseFloat($('input[name="product_price_tax_excl"]').val()),
            tax: parseFloat($('input[name="product_tax"]').val()),
            quantity: parseInt($('input[name="quantity"]').val()),
        },

        cart: {
            totalWithTax: parseInt($('input[name="quantity"]').val()) * (parseFloat($('input[name="product_price_tax_excl"]').val()) + parseFloat($('input[name="product_tax"]').val())),
        },

        updateQuantity: function() {
            context.product.quantity = parseInt($('input[name="quantity"]').val());
            var new_price = context.product.quantity * (context.product.price_tax_excl + context.product.tax);
            context.cart.totalWithTax = new_price;
        },
    };

});

$(document).on("click", ".add_to_cart", function() {

    $.ajax({
        type: 'GET',
        url: base_url+'/ajax/cart/',
        data: {
            product_id: $("input[name='product_id']").val(),
            quantity: $("input[name='quantity']").val(),
        },
        dataType: 'json',
        success: function(data) {
            if(typeof data.error != "undefined") {
                alert(data.error);
            }
            else {
                $("#fancybox_cart .picture img").attr("src", data.product.thumb);
                $("#fancybox_cart .name").html(data.product.name);
                $("#fancybox_cart .unit_price .value").html(parseFloat(context.product.price_tax_excl).formatMoney(2))
                $("#fancybox_cart .quantity .value").html(context.product.quantity)
                $("#fancybox_cart .total_cart .value").html(parseFloat(context.cart.totalWithTax).formatMoney(2))
                $.fancybox($('#add_to_cart_panel').html(), {});
                $(".header .cart .cart_price").html(parseFloat(data.total_cart).formatMoney(2));
                $('input[name="quantity"]').val(1);
            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            console.warn(errorThrown);
        }
    });

    return false;
});

$(document).on("click", ".thumbs img", function(){
    $(".cover-picture img").attr("src", $(this).attr("src"));
});

$(document).on("click", ".quantity .buttons .plus", function(){
    $(".quantity input[name='quantity']").val(parseInt($(".quantity input[name='quantity']").val()) + 1);
    context.updateQuantity();
});

$(document).on("click", ".quantity .buttons .moins", function(){
    if(parseInt($(".quantity input[name='quantity']").val()) > 1) {
        $(".quantity input[name='quantity']").val(parseInt($(".quantity input[name='quantity']").val()) - 1);
        context.updateQuantity();
    }
});

$(document).on("click", ".details .tab", function(){
    event.preventDefault();
    $(".details .tab").removeClass("active");
    $(this).addClass("active");

    $(".details .tab-pane").removeClass("active");
    $($(this).find("a").attr("href")).addClass("active");
});

$(document).on("click", ".product_details .more_infos a", function(){
    event.preventDefault();

    $("a[href='#tab_full_description']").click();

    $('html, body').animate({
        scrollTop:$("#details").offset().top
    }, 'slow');
});

$(document).on("click", "#fancybox_cart .continue_cart", function() {
    $.fancybox.close();
});

$(document).on("click", "#fancybox_cart .pass_order", function() {
    document.location.href = cartLink;
});
