var open_search = false;

$(function () {
    $.ajaxSetup({
        headers: { 'X-CSRF-TOKEN': token }
    });
});

$(document).ready(function(){
	var mySwiper = new Swiper ('.references .swiper-container', {
        nextButton: '.references .button-next',
        prevButton: '.references .button-prev',
        slidesPerView: 12,
        slidesPerGroup: 12,
        simulateTouch: false,
        breakpoints: {
            1024: {
                slidesPerView: 12,
                slidesPerGroup: 12,
                spaceBetween: 10
            },
            768: {
                slidesPerView: 6,
                slidesPerGroup: 6,
                spaceBetween: 10
            },
            640: {
                slidesPerView: 3,
                slidesPerGroup: 3,
                spaceBetween: 10
            }
        }
    });

    var sliderHP = new Swiper ('.slider .swiper-container', {
        nextButton: '.slider .button-next',
        prevButton: '.slider .button-prev',
        pagination: '.slider .swiper-pagination',
        paginationClickable: true,
        simulateTouch: false,
        slidesPerView: 1
    });

    var sliderProduct = new Swiper('.product-slider', {
        nextButton: '.button-next-product',
        prevButton: '.button-prev-product',
        direction: 'vertical',
        slidesPerView: 4,
        simulateTouch: false,
        slidesPerColumnFill: 'column',
        speed: 1000,
        breakpoints: {
            1200: {
                direction: 'horizontal',
                slidesPerColumnFill: 'row',
                spaceBetween: 15,
                slidesPerView: 4,
            },
            425: {
                direction: 'horizontal',
                slidesPerColumnFill: 'row',
                spaceBetween: 15,
                slidesPerView: 2,
            },
        }
    });

    if(open_search)
    {
        $(".search .opened").toggle();
        $(".search .closed").toggle();
    }
});

$(document).on("click", "#toggleMenu", function(){
	$(".menu").toggle();
});

$(document).on("click", ".search .open_close", function(){
    $(".search .opened").toggle();
    $(".search .closed").toggle();
});

$(document).on("click", "#guide", function() {



    $.ajax({
        type: 'GET',
        url: base_url+'/ajax/search/guide',
        success: function(data) {
            console.log(data);
            $.fancybox(data, {});
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            console.warn(errorThrown);
        }
    });

    return false;
});

$(document).on("click", "#guide_panel ul li", function() {
    $(".fiches img").hide();
    $(".fiches").find("#fiche-"+$(this).find("img").data('fiche')).show();
});

$(document).on("click", ".sitemap .header", function(){
    if($(this).parent().find(".content").is(':hidden'))
    {
        $(this).parent().addClass("active");
        $(this).parent().removeClass("no-active");

    }
    else
    {
        $(this).parent().removeClass("active");
        $(this).parent().addClass("no-active");
    }



	$(this).parent().find(".content").slideToggle("slow");
});

function debounce(delay, callback) {
    var timeout = null;
    return function () {
        //
        // if a timeout has been registered before then
        // cancel it so that we can setup a fresh timeout
        //
        if (timeout) {
            clearTimeout(timeout);
        }
        var args = arguments;
        timeout = setTimeout(function () {
            callback.apply(null, args);
            timeout = null;
        }, delay);
    };
}

// Extend the default Number object with a formatMoney() method:
// usage: someVar.formatMoney(decimalPlaces, symbol, thousandsSeparator, decimalSeparator)
// defaults: (2, "$", ",", ".")
Number.prototype.formatMoney = function(places, symbol, thousand, decimal) {
    places = !isNaN(places = Math.abs(places)) ? places : 2;
    symbol = symbol !== undefined ? symbol : " €";
    thousand = thousand || ",";
    decimal = decimal || ".";
    var number = this,
        negative = number < 0 ? "-" : "",
        i = parseInt(number = Math.abs(+number || 0).toFixed(places), 10) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
    return negative + (j ? i.substr(0, j) + thousand : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousand) + (places ? decimal + Math.abs(number - i).toFixed(places).slice(2) : "") + symbol;
};

//# sourceMappingURL=../maps/main.js.map
