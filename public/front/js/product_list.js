$(document).ready(function() {

    if($("#results_search").length > 0) {
        $("#results_search").masonry({
            itemSelector: '.letter',
        });
    }

    product_list = {
        values: {
            appliance_id: $('input[name="appliance_id"]').val(),
            brand_id: $('input[name="brand_id"]').val(),
            category_id: $('input[name="category_id"]').val(),
            page: null,
            search_value: $('input[name="search_value"]').val(),
            recherche: null,
            produits: null,
        },
        init: function() {
            is_initialisation = false;

            hash = document.location.hash.replace('#', '')
            if (hash.length < 1) {
                return
            }

            hash = hash.split('/')
            if (hash.length < 1) {
                return
            }

            needRefesh = false;
            for(var index in hash) {
                parts = hash[index].split('-')

                if (parts.length === 2) {
                    key = parts[0]
                    value = parts[1]
                }
                else {
                    continue
                }

                // Initialisation de la recherche
                if(parts[0] == "recherche") {
                    $('#appliance_search').val(parts[1]);
                    product_list.requestAppliancesLexical();
                }

                if (product_list.values[key] != value) {
                    needRefesh = true
                    product_list.values[key] = value
                }
            }

            if(needRefesh) {
                is_initialisation = true;
                product_list.request();
            }
        },
        request: function() {
            if(product_list.values['produits'] != null && (product_list.values['page'] == 1 || product_list.values['appliance_id'] != product_list.values['produits'])) {
                $.ajax({
                    type: 'GET',
                    url: base_url+'/ajax/appliances/'+product_list.values['produits'],
                    data: product_list.values,
                    success: function(data) {
                        $("#ajax-panel #products-list").remove();
                        $("#ajax-panel .navigation").remove();

                        $("#ajax-panel").append(data);

                        $("#search-panel").hide();

                        product_list.values['appliance_id'] = product_list.values['produits'];

                        product_list.refreshHash();

                        product_list.renderProductList(data);
                        product_list.refreshProductList();
                        product_list.goTopProductList();
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown) {
                        console.warn(errorThrown);
                    }
                });
            }
            else if(product_list.values['page'] > 1) {
                $.ajax({
                    type: 'GET',
                    url: base_url+'/ajax/products',
                    data: product_list.values,
                    dataType: 'json',
                    success: function(data) {
                        product_list.refreshHash();
                        product_list.renderProductList(data);
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown) {
                        console.warn(errorThrown);
                    }
                });
            }
        },
        requestAppliancesLexical: function() {
            $.ajax({
                type: 'GET',
                url: base_url+'/ajax/appliances',
                data: {
                    search_value: $('#appliance_search').val(),
                    brand_id: $('input[name="brand_id"]').val(),
                    category_id: $('input[name="category_id"]').val(),
                },
                success: function(data) {
                    $("#results_search").html(data);

                    $("#search-panel").show();
                    $("#products-list").hide();

                    product_list.values['recherche'] = $('#appliance_search').val();

                    product_list.refreshHash();

                    $("#results_search").masonry('destroy');

                    $("#results_search").masonry({
                        itemSelector: '.letter',
                    });
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    console.warn(errorThrown);
                }
            });
        },
        setValue: function(identifier, value) {
            product_list.values[identifier] = value;
            product_list.request();
        },
        renderProductList: function(data) {

            if(is_initialisation) {
                $(".products").html(data.products);
            }
            else {
                $(".products").append(data.products);
            }

            if(typeof data.products != "undefined") {
                $(".pagination-container").remove();
                $("#products-list").append(data.pagination);
            }

            product_list.refreshProductList();

            is_initialisation = false;
        },
        refreshProductList: function() {
            $("#products-list").hide();
            $("#products-list").fadeIn('slow');
        },
        goTopProductList: function() {
            $('html, body').animate({
                scrollTop: $("#products-list").offset().top
            }, 1000);
        },
        generateHash: function() {
            authorizedHash = ['recherche', 'produits', 'page'];
            hash = []
            for(var index in product_list.values) {
                if (product_list.values[index] && authorizedHash.indexOf(index) != -1) {
                    hash.push(index + '-' + product_list.values[index])
                }
            }

            return '#' + hash.join('/')
        },
        refreshHash: function() {
            document.location.hash = product_list.generateHash();
        },
    };

    product_list.init();

});

$(document).on("click", ".pagination a", function(){
    event.preventDefault();

    var active = parseInt($(".pagination input[name='current_page']").val());
    var page = active + 1;

    product_list.setValue('page', page);
});

$(document).on("click", ".product .add_to_cart", function() {
    var product_id = $(this).closest(".product").find(".product_id").val();

    $.ajax({
        type: 'GET',
        url: base_url+'/ajax/cart/',
        data: {
            product_id: product_id,
            quantity: 1,
        },
        dataType: 'json',
        success: function(data) {
            if(typeof data.error != "undefined") {
                alert(data.error);
            }
            else {
                var totalWithTax = parseFloat(data.product.price_tax_excl) + parseFloat(data.product.tax);
                $("#fancybox_cart .picture img").attr("src", data.product.thumb);
                $("#fancybox_cart .name").html(data.product.name);
                $("#fancybox_cart .unit_price .value").html(parseFloat(data.product.price_tax_excl).formatMoney(2))
                $("#fancybox_cart .quantity .value").html(1)
                $("#fancybox_cart .total_cart .value").html(parseFloat(totalWithTax).formatMoney(2))
                $.fancybox($('#add_to_cart_panel').html(), {});
                $(".header .cart .cart_price").html(parseFloat(data.total_cart).formatMoney(2));
            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            console.warn(errorThrown);
        }
    });

    return false;

});

$(document).on("click", "#fancybox_cart .continue_cart", function() {
    $.fancybox.close();
});

$(document).on("click", "#fancybox_cart .pass_order", function() {
    document.location.href = cartLink;
});
