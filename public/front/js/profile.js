$(document).ready(function(){
    $('.addresses .delete').on("click", function(){
        event.preventDefault();
        var confirm_value = confirm("Êtes-vous sûr ?");

        if(confirm_value == true) {
            location.href = $(this).attr("href");
        }
    });

    if($("#resume_order").length != 0)
    {
        $('html, body').animate({
            scrollTop:  $("#resume_order").offset().top
        }, 'slow');
    }

    displayProfessionnalFields();
    $("input[name='pro']").click(function() {
        displayProfessionnalFields();
    });
});

function displayProfessionnalFields()
{
    if($("input[name='pro']:checked").val() == 1)
    {
        $(".company").fadeIn("slow");
        $(".vat_number").fadeIn("slow");
    }
    else
    {
        $(".company").fadeOut("slow");
        $(".vat_number").fadeOut("slow");
    }
}
