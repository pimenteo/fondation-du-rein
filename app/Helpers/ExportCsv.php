<?php

namespace App\Helpers;

use League\Csv\Writer as LeagueCsvWriter;
use League\Csv\Reader as LeagueCsvReader;

class ExportCsv
{
	private $_csv;
    private $_filename;

    public function __construct($filename = 'export')
    {
        $this->_filename = $filename;
        $this->_csv      = LeagueCsvWriter::createFromFileObject(new \SplTempFileObject());
    }

    public function addLine($line = [])
    {
        $this->_csv->insertOne($line);
    }

    public function render()
    {
        $this->_csv->setInputEncoding('ISO-8859-15');
        $this->_csv->setOutputBOM(LeagueCsvReader::BOM_UTF8);
        $this->_csv->output($this->_filename.'.csv');
        return;
    }

}