<?php

namespace App\Helpers;

use Illuminate\Http\Request;

trait Searchform
{

    private $_searchfields  = [];

    private $_orderfields   = [];

    private $_submitbuttons = [];

    private $_search        = [];


    protected function buildSearchForm( $searchfields, Request $request )
	{
        if (empty($searchfields)) {
            return null;
        }

		if (isset($this->actionUrl)) {
			$this->_actionUrl = $this->actionUrl;
		}

        $this->_searchfields = $searchfields;

        $object = $this->getModelInstance();

        if (!isset($this->showOrderFields) || $this->showOrderFields) {
    		if ( method_exists($object, 'getOrderFieldset')
                && is_callable([$object, 'getOrderFieldset']) )
            {
    			$this->_orderfields = $this->getModelInstance()->getOrderFieldset();
            } else {
    			$this->generateOrderFieldset();
            }
        }

        $this->populateSearchFields( $request );
        $this->populateOrderFields( $request );

		return view('helpers.searchform', [
            'actionUrl'  	=> $this->actionUrl,
            'searchfields'  => $this->_searchfields,
            'orderfields'   => $this->_orderfields,
            'submitButtons' => $this->_submitbuttons,
            'search'        => $this->_search
        ]);
	}

	private function generateOrderFieldset()
	{
		$options = [];
        foreach ($this->_searchfields as $fieldKey => $field) {
            $options[$fieldKey] = $field['label'];
        }

        $this->_orderfields = [
            'sortCol' => [
                'type'      => 'equal',
                'label'     => 'Trier par',
                'options'   => $options,
                'value'     => null,
            ],
            'sortDir' => [
                'type'      => 'equal',
                'label'     => 'Direction',
                'options'   => [
                    'asc'  => 'Croissant',
                    'desc' => 'Décroissant'
                ],
                'value'     => null,
            ],
        ];
	}

    private function populateSearchFields(Request $request) {
        if (! $request->search || ! is_array($request->search)) {
            return false;
        }

        foreach ($request->search as $fieldName => $search) {
            foreach ($search as $searchType => $values) {
                if ( ! $this->_searchfields || ! $this->_searchfields[$fieldName] || ! array_key_exists('value', $this->_searchfields[$fieldName]) ) {
                    continue;
                }

                if ( is_null($values) ) {
                    continue;
                }                

                if (is_string($values) && strlen($values) <= 0) {
                    continue;
                }

                $this->_searchfields[$fieldName]['value'] = $values;
                $this->_search[$fieldName] = $values;
            }
        }
    }

    private function populateOrderFields(Request $request) {
        if (! $request->sortCol || ! $request->sortDir) {
            return false;
        }

        foreach ($this->_orderfields as $fieldName => &$field) {
            if (! $request->{$fieldName}) {
                continue;
            }

            $field['value'] = $request->{$fieldName};
        }
    }

	public function setSubmitButtons( $submitButtons = [] ) {
		$this->_submitbuttons = $submitButtons;
	}

}
