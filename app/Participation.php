<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Participation extends Model
{
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $fillable = [ 'contact_id', 'gala_id', 'invite_de', 'service_rendu', 'relance', 'category', 'mot_a_joindre', 'carton_entreprise', 'paiement_place', 'nb_place', 'nb_repas', 'invited', 'confirmed' ];


	/***** lists *****/
	public function getCategoryTypes() 
	{
		return [
			'payant' 	=> 'Payant',
			'invite' 	=> 'Invité',
		];
	}
	

	// belongs to

	/**
     * Get the participations's contact.
     */
	public function contact() 
	{
		return $this->belongsTo('App\Contact');
	}

	/**
     * Get the participations's gala.
     */
	public function gala() 
	{
		return $this->belongsTo('App\Gala');
	}


	/***** mutators *****/
	public function setRelanceAttribute($value) {
		$this->attributes['relance'] = is_null($value) ? 0 : $value;
	}

	public function setCartonEntrepriseAttribute($value) {
		$this->attributes['carton_entreprise'] = is_null($value) ? 0 : $value;
	}

	public function setPaiementPlaceAttribute($value) {
		$this->attributes['paiement_place'] = is_null($value) ? 0 : $value;
	}

	public function setInvitedAttribute($value) {
		$this->attributes['invited'] = is_null($value) ? 0 : $value;
	}
	
	public function setConfirmedAttribute($value) {
		$this->attributes['confirmed'] = is_null($value) ? 0 : $value;
	}


	/* Scope a query to only include or exclude affaire with en cours fournitures.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeInLastGala($query)  
    {
        return $query->where('gala_id', '=', Gala::max('id'));
    }
	

	public function scopeDepthLike($query, $field, $value) {
		return $query->where($field, 'like', '%'.$value.'%');
	}
}
