<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Contact;
use DB;

class Gala extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $fillable = ['name', 'date'];


    /**
     * The attributes that should be casted to native types.
     *
     * @type array
     */
    protected $dates = [
        'date',
        'created_at', 
        'updated_at',
    ];
    

	// has many
	
	/**
     * Get all the gala participations.
     */
    public function participations()
    {
        return $this->hasMany('App\Participation');
    }

    /***** mutators *****/
    public function setDateAttribute($value)
    {
        $this->attributes['date'] = strlen($value) > 0 ? \Carbon\Carbon::createFromFormat('d/m/Y', $value) : \Carbon\Carbon::now();
    }

    public function getNotInvited($count = false) {
        $subQuery = DB::table('participations')
            ->where('gala_id', $this->id)
            ->select(['contact_id']);
        
        if ($count) {
            return Contact::whereNotIn('id', $subQuery)->count();
        }
        return Contact::whereNotIn('id', $subQuery)->get();
    }

    public static function getList($emptyLine = false) {
        $list = Gala::all()->pluck('name', 'id')->toArray();

        return $emptyLine ? ['' => 'Tous les événements'] + $list : $list;
    }
}
