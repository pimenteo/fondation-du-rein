<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

	// public static function getSearchFieldset($key = null) {
	// 	$fieldset =  [
 //            'name' => [
	// 			'like' => '',
	// 		],
	// 		'email' => [
	// 			'like' => '',
	// 		],
 //        ];

 //        if (is_string($key)) {
 //            return array_key_exists($key, $fieldset) ? $fieldset[$key] : null;
 //        }
 //        if (is_array($key)) {
 //            return array_only($fieldset, $key);
 //        }

 //        return $fieldset;
	// }
}
