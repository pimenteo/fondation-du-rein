<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Contact extends Model
{

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $fillable = ['civility', 'gender', 'society', 'particule', 'firstname', 'lastname', 'email', 'profession', 'address_1', 'address_2', 'postcode', 'city', 'country', 'telephone_1', 'telephone_2', 'birthdate'];


    /**
     * The attributes that should be casted to native types.
     *
     * @type array
     */
    protected $dates = [
        'birthdate',
        'created_at', 
        'updated_at',
    ];


    /* boot method */
    // public function boot()
    // {

    // }


	/***** has_many *****/

    /**
     * Get all the contact participations.
     */
    public function participations()
    {
        return $this->hasMany('App\Participation');
    }

    public function lastParticipation() {
        return $this->hasOne('App\Participation')->orderBy('id', 'desc');
    }

    public function lastestParticipation() 
    {
        return $this->hasOne('App\Participation')->inLastGala();
    }

    /**
     * Get all the contact dons.
     */
    public function dons()
    {
        return $this->hasMany('App\Don');
    }


	/* Scope a query to only include or exclude affaire with en cours fournitures.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWasInLastGala($query)  
    {
        return $query->has('lastestParticipation');
    }

    public function scopeByEvent($query, $eventId) 
    {
        return $query->with(['participations', function($q) use($eventId) {
            $q->where('gala_id', $eventId);
        }]);
    }

    public function scopeByInviting($query, $value) 
    {
        return $query->whereHas('participations', function($q) use($value) {
            $q->depthLike('invite_de', (string)$value);
        });
    }

    public function scopeByRelance($query, $value) {
        return $query->whereHas('participations', function($q) use($value) {
            $q->depthLike('relance', (string)$value);
        });
    }

    public function scopeByCategory($query, $value) {
        return $query;
    }

    public function scopeByMot($query, $value) {
        return $query->whereHas('participations', function($q) use($value) {
            $q->depthLike('mot_a_joindre', (string)$value);
        });
    }

    public function scopeByCarton($query, $value) {
        return $query->whereHas('participations', function($q) use($value) {
            $q->depthLike('carton_entreprise', (string)$value);
        });
    }

    public function scopeHavePaid($query, $value = 1) {
        return $query->with(['lastestParticipation' => function($q) use($value) {
            $q->where('paiement_place', ($value ? '>': '<='), 0);
        }]);
    }

    public function scopeHaveConfirmed($query, $value = 1) {
        return $query->with(['lastestParticipation' => function($q) use($value) {
            $q->where('confirmed', '=', $value);
        }]);
    }



	/***** mutators *****/
    public function setBirthdateAttribute($value)
    {
        $this->attributes['birthdate'] = strlen($value) > 0 ? \Carbon\Carbon::createFromFormat('d/m/Y', $value) : \Carbon\Carbon::now();
    }

    public function getFullNameAttribute()
    {
        return $this->civility . ' ' . ucfirst($this->lastname) . ' ' . $this->particule . ' ' .  ucfirst($this->firstname);
    }
    
    public function getTotalDonsAttribute()
    {
        if ($this->dons->isEmpty()) {
            return number_format(floatval(0), 2)." €";
        }

        return $this->dons->reduce(function ($total, $item) {
            return number_format(floatval(($total + $item->amount)), 2)." €";        
        });
    }

    public function getInviteDeAttribute() 
    {
        if (!is_null($this->lastestParticipation)) {
            return $this->lastestParticipation->invite_de;
        }

        return '---';
    }

    // invited mutators
    public function getIsInvitedInLastGalaAttribute() 
    {
        return !(is_null($this->lastestParticipation) || !$this->lastestParticipation->invited);
    }
    
    public function getInvitedInLastGalaAttribute() 
    {
        return $this->is_invited_in_last_gala ? 'Oui' : 'Non';
    }

    public function getCartonEntrepriseAttribute() 
    {
        return $this->lastestParticipation->carton_entreprise ? 'Oui' : 'Non';
    }

    public function getMotAJoindreAttribute() 
    {
        return !is_null($this->lastestParticipation->mot_a_joindre) ? $this->lastestParticipation->mot_a_joindre : '';
    }

    public function getCategoryAttribute() 
    {
        return ucfirst($this->lastestParticipation->category);
    }

    public function getPaiementPlaceAttribute() 
    {
        return $this->lastestParticipation->paiement_place ? 'Oui' : 'Non';
    }

    public function getInvitedBtnAttribute() 
    {
        if (is_null($this->lastestParticipation)) {
            return 'Non';
        }

        $url    = url('contacts/invite/'.$this->id);
        $class  = ($this->is_invited_in_last_gala) ? 'btn-success' : 'btn-danger';

        return '<a href="' . $url . '" class="btn btn-xs ' . $class . ' btn-fill">' . $this->invited_in_last_gala . '</a>';
    }

    // confirmed mutators
    public function getIsConfirmedInLastGalaAttribute() 
    {
        return !(is_null($this->lastestParticipation) || !$this->lastestParticipation->confirmed);
    }

    public function getConfirmedInLastGalaAttribute() 
    {
        return $this->is_confirmed_in_last_gala ? 'Oui' : 'Non';
    }

    public function getConfirmedBtnAttribute() 
    {
        if (is_null($this->lastestParticipation)) {
            return 'Non';
        }

        $url    = url('contacts/confirm/'.$this->id);
        $class  = ($this->is_confirmed_in_last_gala) ? 'btn-success' : 'btn-danger';

        return '<a href="' . $url . '" class="btn btn-xs ' . $class . ' btn-fill">' . $this->confirmed_in_last_gala . '</a>';
    }


    public function getFullAddressAttribute() {
        return $this->address_1 .' '. $this->address_2;
    }

    public function getFreeCivilityAttribute() {
        if(!in_array($this->civility, $this->getCivilityTypes())) {
            return $this->civility;
        }
        return "";
    }

    /***** lists *****/
	public function getCivilityTypes() 
	{
		return [
			'' 		=> '---',
			'Madame' 	=> 'Madame',
			'Mademoiselle' 	=> 'Mademoiselle',
            'Monsieur'    => 'Monsieur',
            'Monsieur et Madame'    => 'Monsieur et Madame',
            'Docteur'    => 'Docteur',
            'Madame le Docteur'    => 'Madame le Docteur',
            'Le Docteur et Madame'    => 'Le Docteur et Madame',
            'Madame le Professeur'    => 'Madame le Professeur',
            'Le Professeur et Madame'    => 'Le Professeur et Madame',
            'Le Comte et la Comtesse'    => 'Le Comte et la Comtesse',
            'Le Baron et la Baronne'    => 'Le Baron et la Baronne',
		];
	}

	public function getGenderTypes() 
	{
		return [
			'' 		=> '---',
			'M'		=> 'Homme',
			'F'		=> 'Femme',
		];
	}

    public static function getList($emptyLine = false) {
        $list = self::all()->pluck('full_name', 'id')->toArray();

        return $emptyLine ? ['' => 'Tous les contacts'] + $list : $list;
    }


    /***** form helper *****/
    public function getSearchFieldset($key = null) {
        $fieldset =  [
            'firstname'     => [
                'type'  => 'like',
                'label' => 'Nom',
                'value' => null,
            ],
            'postcode'      => [
                'type'  => 'like',
                'label' => 'Code postal',
                'value' => null,
            ],
        ];

        if (is_string($key)) {
            return array_key_exists($key, $fieldset) ? $fieldset[$key] : null;
        }
        if (is_array($key)) {
            return array_only($fieldset, $key);
        }

        return $fieldset;
    }


    /***** requesting functions *****/
    public static function getResults( Request $request) {

        $searchTerms = $request->search;

        if (is_null($searchTerms)) {
            return self::with(['participations', 'dons']);
        }

        $subquery = DB::table('participations')->select('contact_id');
        foreach ($searchTerms as $key => $value) {
            if ($key == 'gala_id' && isset($value['equal']) && $value['equal'] > 0) {
                $subquery->where($key, '=', $value['equal']);
            } elseif ($key != 'gala_id' && isset($value['equal']) && strlen($value['equal']) > 0) {
                $subquery->where($key, '=', $value['equal']);
            } elseif (isset($value['like']) && strlen($value['like']) > 0) {
                $subquery->where($key, 'like', '%'.$value['like'].'%');
            } elseif (isset($value['bool']) && !is_null($value['bool']) && $value['bool'] != '') {
                if ($key == 'paiement_place') {
                    $subquery->where($key, ($value['bool'] ? '>' : '='), 0);
                } else {
                    $subquery->where($key, $value['bool']);
                }
            }
        }

        $list_ids = array();

        foreach ($subquery->get() as $participant) {
            $list_ids[] = $participant->contact_id;
        }

        return self::with(['participations', 'dons'])->whereIn('id', $list_ids);          
    }
}
