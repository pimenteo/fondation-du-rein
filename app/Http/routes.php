<?php

Route::group(['prefix' => 'auth', 'namespace' => 'Auth'], function () {
	Route::get('login',		'AuthController@getLogin');
    Route::post('login',	'AuthController@postLogin');
    Route::get('logout',	'AuthController@getLogout');
});


Route::group(['middleware' => 'auth', 'namespace' => 'Admin'], function () {
	Route::get('/', 						'ContactsController@index');

	Route::get('/statistiques', 			'DashboardController@index');
	Route::get('/home', 					'DashboardController@index');

	Route::any('contacts/search', 			'ContactsController@index');
	Route::any('statistiques/search', 		'DashboardController@index');
	Route::any('statistiques/dons/search', 	'StatsdonsController@index');

	Route::resource('contacts', 			'ContactsController', ['except' => ['edit']]);
	Route::get('contacts/invite/{id}', 		'ContactsController@invite');
	Route::get('contacts/confirm/{id}', 	'ContactsController@confirm');

	Route::resource('evenements', 			'EvenementsController', ['except' => ['edit']]);
	Route::get('evenements/invite/{id}', 	'EvenementsController@invite');

	Route::resource('administrators', 		'AdministratorsController', ['except' => ['edit']]);


	Route::group(['namespace' => 'Contacts'], function () {
		Route::resource('contacts.participations', 'ParticipationsController', ['except' => ['index', 'edit']]);
		Route::resource('contacts.dons', 'DonsController', ['except' => ['index', 'show', 'update', 'edit']]);
	});

});
