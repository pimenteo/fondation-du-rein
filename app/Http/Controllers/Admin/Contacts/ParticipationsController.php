<?php

namespace App\Http\Controllers\Admin\Contacts;

use App\Participation;
use App\Gala;
use App\Http\Controllers\AdminController;
use Illuminate\Database\Eloquent\Builder as QueryBuilder;
use Illuminate\Http\Request;

class ParticipationsController extends AdminController
{
    const MODEL = Participation::class;
    protected $with = [];

    protected $name = 'participations';

    protected $validation = [
		'gala_id' 			=> 'required|integer',
		'invite_de' 		=> 'string|max:255',
		'service_rendu' 	=> 'string|max:255',
		'relance' 			=> 'boolean',
		'category' 			=> 'string|in:payant,invite',
		'mot_a_joindre' 	=> 'string|max:255',
		'carton_entreprise' => 'boolean',
		'paiement_place' 	=> 'boolean',
		'nb_place' 			=> 'integer',
		'nb_repas' 			=> 'integer',
        'invited'           => 'boolean',
		'confirmed' 		=> 'boolean',
    ];

    /**
     * Override pour ajout des valeurs par défaut.
     */
    public function create()
    {
    	$this->boot();

        $object = $this->getModelInstance();

        $object->contact_id = (int)Request()->segment(2);

        $this->setDatas('model', $object);
        $this->setDatas('lstGalas', Gala::lists('name', 'id'));

        return $this->render();
    }

    /**
     * Override pour enregistrement des valeurs par défaut.
     */
    public function store(Request $request)
    {
        $this->boot();

        $model = static::MODEL;

        $this->validate($request, $this->validation, $this->messages, $this->attributes);

        $attributes = $request->only(array_keys($this->validation));
        $attributes['contact_id'] = (int)Request()->segment(2);
        
        $object = $model::create($attributes);
        $object->load($this->with);

        if (!is_null($object)) {
            return redirect()->route( 'contacts.show', $object->contact_id )->with('success', 'Modification enregistrée');
        }

        return $object;
    }


    /**
     * Override pour récupération participation.
     */
    public function show($id)
    {
        $this->boot();

        $model = static::MODEL;

        $requestId = (int)Request()->segment(4);

        $object = $model::with($this->with)->findOrFail($requestId);

        $this->setDatas('model', $object);
        $this->setDatas('lstGalas', Gala::lists('name', 'id'));

        return $this->render();
    }

    /**
     * Override pour update participation.
     */
    public function update(Request $request, $id)
    {
        $this->boot();

        $model = static::MODEL;

        $requestId = (int)Request()->segment(4);

        $this->validate($request, $this->validation, $this->messages, $this->attributes);

        $attributes =  $request->only(array_keys($this->validation));
        if (array_key_exists('id', $attributes)) {
            unset($attributes['id']);
        }

        $object = $model::findOrFail($requestId);
        $object->fill($attributes)->save();

        $object->load($this->with);

        if (!is_null($object)) {
            return redirect()->route( 'contacts.show', $object->contact_id )->with('success', 'Modification enregistrée');
        }

        return $object;
    }


    /**
     * Override pour suppression
     */
    public function destroy($id)
    {
        $this->boot();

        $model = static::MODEL;

        $requestId = (int)Request()->segment(4);

        $model::where('contact_id', '=', $id)->where('id', '=', $requestId)->firstOrFail()->delete();

        return redirect()->route( 'contacts.show', $id )->with('success', 'Element supprimé');
    }

}