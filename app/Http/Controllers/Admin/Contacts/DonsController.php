<?php

namespace App\Http\Controllers\Admin\Contacts;

use App\Don;
use App\Http\Controllers\AdminController;
use Illuminate\Database\Eloquent\Builder as QueryBuilder;
use Illuminate\Http\Request;

class DonsController extends AdminController
{
    const MODEL = Don::class;
    protected $with = [];

    protected $name = 'dons';

    protected $validation = [
		'amount'		=> 'numeric',
		'date_don'		=> 'required|date_format:d/m/Y',
		'payment'		=> 'required|string',
		'marketing'		=> 'required|string',
    ];

     /**
     * Override pour ajout des valeurs par défaut.
     */
    public function create()
    {
    	$this->boot();

        $object = $this->getModelInstance();

        $object->contact_id = (int)Request()->segment(2);

        $this->setDatas('model', $object);

        return $this->render();
    }

    /**
     * Override pour enregistrement des valeurs par défaut.
     */
    public function store(Request $request)
    {
        $this->boot();

        $model = static::MODEL;

        $this->validate($request, $this->validation, $this->messages, $this->attributes);

        $attributes = $request->only(array_keys($this->validation));
        $attributes['contact_id'] = (int)Request()->segment(2);
        
        $object = $model::create($attributes);
        $object->load($this->with);

        if (!is_null($object)) {
            return redirect()->route( 'contacts.show', $object->contact_id )->with('success', 'Modification enregistrée');
        }

        return $object;
    }

    /**
     * Override pour suppression
     */
    public function destroy($id)
    {
        $this->boot();

        $model = static::MODEL;

        $requestId = (int)Request()->segment(4);

        $model::where('contact_id', '=', $id)->where('id', '=', $requestId)->firstOrFail()->delete();

        return redirect()->route( 'contacts.show', $id )->with('success', 'Element supprimé');
    }
}