<?php

namespace App\Http\Controllers\Admin;

use DB;
use App\Don;
use App\Http\Controllers\AdminController;
use Illuminate\Database\Eloquent\Builder as QueryBuilder;
use Illuminate\Http\Request;
use \App\Helpers\Searchform;
use \App\Exports\Dons as ExportDons;

class StatsdonsController extends AdminController
{
	use \App\Helpers\Searchform;

	const MODEL = Don::class;

	protected $actionUrl 		= 'statistiques/dons/search';

	protected $searchForm = [];

	protected $submitbuttons = [
		'Rechercher' => [
			'name'		=> 'submit',
			'type' 		=> 'submit',
			'class'		=> 'btn btn-primary btn-fill',
		],
		'Exporter' => [
			'name'		=> 'export',
			'type' 		=> 'submit',
			'class'		=> 'btn btn-success btn-fill'
		],
	];

	protected function boot() {
		parent::boot();

		$this->searchForm = [

		];

		$this->setSubmitButtons( $this->submitbuttons );
	}

	public function index(Request $request) {
		$this->boot();

		return $this->handleRender($request);
    }

    private function handleRender(Request $request) {
    	$object = $this->getModelInstance();

        $query = $object->query();

        if (is_null($request->fields)) {
            $query->with($this->with);
        }

        $query = $this->handleFieldsParameters($query, $request);
        $query = $this->handleOrderByParameters($query, $request);
        $query = $this->handleSearchParameters($query, $request);


        if ( method_exists($object, 'getSearchFieldset')
            && is_callable([$object, 'getSearchFieldset']) )
        {
            $this->setDatas('searchForm', $this->buildSearchForm( $object->getSearchFieldset(), $request ));
        }
    	
    	if (isset($request->export)) {
        	$export = new ExportDons( $query->get() );
        	return $export->render();
        }

        $query = $this->handlePaginationParameters($query, $request);
    	$this->setDatas('datas', $query->items());
    	$this->setDatas('query', $query);
    	
		return $this->render();
    }

}