<?php

namespace App\Http\Controllers\Admin;

use App\Gala;
use App\Http\Controllers\AdminController;
use Illuminate\Database\Eloquent\Builder as QueryBuilder;
use Illuminate\Http\Request;

class EvenementsController extends AdminController
{
    const MODEL = Gala::class;
    protected $with = [];

    protected $name = 'evenements';

    protected $validation = [
		'name' 	=> 'required|string|max:255',
		'date' 	=> 'required|date_format:d/m/Y',
    ];

    public function invite($id)
    {
    	$object = Gala::findOrFail($id);

    	$contacts = $object->getNotInvited();
    	if ($contacts->isEmpty()) {
    		return redirect()->route($this->name.'.index')->with('danger', 'Pas de contact à inviter');
    	}

    	$contacts->each(function($contact) use($object) {
    		try {
    			$lastParticipation = $contact->lastParticipation;
    			
    			$contact->participations()->create([
    				'gala_id' 			=> $object->id,
    				'invite_de' 		=> isset($lastParticipation) ? $lastParticipation->invite_de : '',
    				'relance' 			=> isset($lastParticipation) ? $lastParticipation->relance : '',
    				'category' 			=> isset($lastParticipation) ? $lastParticipation->category : '',
    				'mot_a_joindre' 	=> isset($lastParticipation) ? $lastParticipation->mot_a_joindre : '',
    				'carton_entreprise' => isset($lastParticipation) ? $lastParticipation->carton_entreprise : '',
    				'paiement_place' 	=> isset($lastParticipation) ? $lastParticipation->paiement_place : '',
    				'nb_place' 			=> isset($lastParticipation) ? $lastParticipation->nb_place : '',
    				'nb_repas' 			=> isset($lastParticipation) ? $lastParticipation->nb_repas : '',
				]);

    		} catch (\Exception $e) {
				// var_dump($e->getMessage());
                return ['status' => false, 'message' => $e->getMessage()];
            } finally {
                return true;
            }
    	});

    	return redirect()->route($this->name.'.index')->with('success', 'Contacts invités');
    }
}
