<?php

namespace App\Http\Controllers\Admin;

use App\Contact;
use App\Http\Controllers\AdminController;
use Illuminate\Database\Eloquent\Builder as QueryBuilder;
use Illuminate\Http\Request;
use \App\Helpers\Searchform;

class ContactsController extends AdminController
{
	use \App\Helpers\Searchform;

    const MODEL = Contact::class;
    protected $with = ['participations', 'lastestParticipation', 'dons'];

	protected $actionUrl = 'contacts/search';

    protected $name = 'contacts';

    protected $validation = [
        'civility'      => 'string',
    	'free_civility' => 'string',
		'gender' 		=> 'string|in:M,F',
		'society' 		=> 'string|max:255',
		'particule' 	=> 'string|max:255',
		'firstname' 	=> 'string|max:255',
		'lastname' 		=> 'string|max:255',
		'email' 		=> 'string|max:255',
		'profession' 	=> 'string|max:255',
		'address_1' 	=> 'string|max:255',
		'address_2' 	=> 'string|max:255',
		'postcode' 		=> 'string|max:255',
		'city' 			=> 'string|max:255',
		'country' 		=> 'string|max:255',
		'telephone_1' 	=> 'string|max:255',
		'telephone_2' 	=> 'string|max:255',
		'birthdate' 	=> 'date_format:d/m/Y',
    ];

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->boot();

        $model = static::MODEL;

        $this->validate($request, $this->validation, $this->messages, $this->attributes);

        $attributes = $request->only(array_keys($this->validation));

        if(empty($attributes['civility'])) {
            $attributes['civility'] = $request->input('free_civility');
        }

        $object = $model::create($attributes);
        $object->load($this->with);

        if (!is_null($object)) {
            return redirect()->route($this->name.'.index')->with('success', 'Modification enregistrée');
        }

        return $object;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->boot();

        $model = static::MODEL;

        $this->validate($request, $this->validation, $this->messages, $this->attributes);

        $attributes =  $request->only(array_keys($this->validation));
        if (array_key_exists('id', $attributes)) {
            unset($attributes['id']);
        }

        if(empty($attributes['civility'])) {
            $attributes['civility'] = $request->input('free_civility');
        }

        $object = $model::findOrFail($id);
        $object->fill($attributes)->save();

        $object->load($this->with);

        if (!is_null($object)) {
            return redirect()->route($this->name.'.index')->with('success', 'Modification enregistrée');
        }

        return $object;
    }

    public function invite($id) 
    {
    	$object = Contact::with('lastestParticipation')->findOrFail($id);

    	if (is_null($object->lastestParticipation)) {
    		return redirect()->route($this->name.'.index')->with('danger', 'Le contact ne participe pas au dernier gala.');
    	}

    	$object->lastestParticipation->invited = !$object->lastestParticipation->invited;
    	$object->lastestParticipation->save();

		return redirect()->route($this->name.'.index')->with('success', 'Confirmation mise à jour');    	
    }

    public function confirm($id) 
    {
    	$object = Contact::with('lastestParticipation')->findOrFail($id);

		if (is_null($object->lastestParticipation)) {
    		return redirect()->route($this->name.'.index')->with('danger', 'Le contact ne participe pas au dernier gala.');
    	}

    	$object->lastestParticipation->confirmed = !$object->lastestParticipation->confirmed;
    	$object->lastestParticipation->save();

		return redirect()->route($this->name.'.index')->with('success', 'Confirmation mise à jour');    	
    }
}
