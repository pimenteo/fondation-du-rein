<?php

namespace App\Http\Controllers\Admin;

use App\Contact;
use App\Gala;
use App\Http\Controllers\AdminController;
use Illuminate\Database\Eloquent\Builder as QueryBuilder;
use Illuminate\Http\Request;
use \App\Helpers\Searchform;
use \App\Exports\Contacts as ExportContacts;

class DashboardController extends AdminController
{
	use \App\Helpers\Searchform;

	const MODEL = Contact::class;
	protected $with = ['participations', 'lastestParticipation', 'dons'];

	protected $actionUrl 		= 'statistiques/search';
	protected $showOrderFields 	= false;

	protected $searchForm = [];

	protected $submitbuttons = [
		'Rechercher' => [
			'name'		=> 'submit',
			'type' 		=> 'submit',
			'class'		=> 'btn btn-primary btn-fill',
		],
		'Exporter' => [
			'name'		=> 'export',
			'type' 		=> 'submit',
			'class'		=> 'btn btn-success btn-fill'
		],
	];

	protected function boot() {
		parent::boot();

		$this->searchForm = [
			'gala_id' => [
				'type'  => 'equal',
				'label' => 'Evenement',
				'options'	=> Gala::getList(true),
				'value' => null,
			],
			'invite_de' => [
				'type'  => 'like',
				'label' => 'Invité de',
				'value' => null,
			],
			'relance' => [
				'type'  => 'bool',
				'label' => 'Relance',
				'value' => '',
			],
			'category' => [
				'type'  => 'equal',
				'label' => 'Catégorie',
				'options' => [
					'' 		 => 'Toutes',
					'payant' => 'Payant',
					'invite' => 'Invité',
				],
				'value' => null,
			],
			'mot_a_joindre' => [
				'type'  => 'like',
				'label' => 'Mot à joindre par',
				'value' => null,
			],
			'carton_entreprise' => [
				'type'  => 'bool',
				'label' => 'Carton entreprise',
				'value' => '',
			],
			'paiement_place' => [
				'type'  => 'bool',
				'label' => 'Paiement de place',
				'value' => '',
			],
			'invited' => [
				'type'  => 'bool',
				'label' => 'Invitation envoyée',
				'value' => '',
			],
			'confirmed' => [
				'type'  => 'bool',
				'label' => 'Participation confirmée',
				'value' => '',
			],
		];

    	$this->setSubmitButtons( $this->submitbuttons );
	}

    public function index(Request $request) {
    	$this->boot();

        $this->setDatas('searchForm', $this->buildSearchForm( $this->searchForm, $request ));

        return $this->handleRender($request);
    }

    private function handleRender(Request $request) {
    	$model = static::MODEL;
    	
    	$query = $model::getResults($request);
    	
    	if (isset($request->export)) {
        	$export = new ExportContacts( $query->get() );
        	return $export->render();
        }

        $query = $this->handlePaginationParameters($query, $request);

    	$this->setDatas('datas', $query->items());
    	$this->setDatas('query', $query);

		return $this->render();
    }

}
