<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\User;
use Illuminate\Http\Request;

class AdministratorsController extends AdminController
{    
    const MODEL = User::class;

    protected $validation = [
        'email'            => 'required|email|unique:users,email',
        'name'        => 'required|string',
        'password'         => 'string',
        'password_confirm' => 'string|required_with:password|same:password',
    ];

    protected $attributes = [
        'password'         => 'Mot de passe',
        'password_confirm' => 'Confirmation du mot de passe',
    ];

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->boot();

        $model = static::MODEL;

        $this->validation['password'] = 'required|string';
        $this->validation['password_confirm'] = 'required|string|required_with:password|same:password';

        $this->validate($request, $this->validation, $this->messages, $this->attributes);

        $attributes = $request->only(array_keys($this->validation));

        $attributes['password'] = bcrypt($attributes['password']);

        $object = $model::create($attributes);
        $object->load($this->with);

        if (!is_null($object)) {
            return redirect()->route($this->name.'.index')->with('success', 'Modification enregistrée');
        }

        return $object;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->boot();

        $model = static::MODEL;

        $this->validation['email'] .= ','.$id;

        $this->validate($request, $this->validation, $this->messages, $this->attributes);

        $attributes =  $request->only(array_keys($this->validation));
        if (array_key_exists('id', $attributes)) {
            unset($attributes['id']);
        }

        if(!empty($attributes['password'])) {
            $attributes['password'] = bcrypt($attributes['password']);
        }
        else {
            unset($attributes['password']);
        }

        $object = $model::findOrFail($id);
        $object->fill($attributes)->save();

        $object->load($this->with);

        if (!is_null($object)) {
            return redirect()->route($this->name.'.index')->with('success', 'Modification enregistrée');
        }

        return $object;
    }
}
