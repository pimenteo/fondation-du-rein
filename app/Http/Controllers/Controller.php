<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

abstract class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $datas   = array();

    protected $route;

    protected $name;



    protected function boot() 
    {
        $this->route = $this->cleanRoute( \Route::currentRouteAction() );

        $route = explode('.', $this->route);
        array_pop($route);

        $this->name = end($route);

        $this->setDatas('bodyId', $this->name);
    }

    protected function setDatas($text, $data) {
    	$this->datas[$text] = $data;
    }

    protected function render() {
    	return view($this->route, $this->datas);
    }

    private function cleanRoute($route, $returnArray = false)
    {
        // Format a proper route for view to use
        $cleanedRoutePath = [];

        $route         = str_replace('_', '.', $route);
        $routeParts    = explode('@', $route);

        // cleaning base route path
        $baseRouteParts = explode('\\', $routeParts[0]);
        foreach ($baseRouteParts as $segment) {
            if (!in_array($segment, ['App', 'Http', 'Controllers'])) {
                $cleanedRoutePath[] = $segment;
            }
        }

        // cleaning endpoint
        $cleanedRoutePath[] = preg_replace('/^post/', '', preg_replace('/^get/', '', $routeParts[1]));

        $route = strtolower(str_replace('Controller', '', implode('.', $cleanedRoutePath)));
        if ($returnArray) {
            $cleanedRoutePath   = explode('.', $route);
        }

        // If the route still does not exist, throw a 404
        if (!\View::exists($route)) {
            if (\Config::get('app.debug') == false) {
                App::abort(404);
            }
        }

        return $route;
    }
}
