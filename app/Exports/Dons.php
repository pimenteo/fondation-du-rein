<?php

namespace App\Exports;

class Dons extends \App\Helpers\ExportCsv
{

	public function __construct($dons = null, $filename = 'exportContacts')
	{
		parent::__construct( $filename );

		// header
        $this->addLine([
            'Nom complet',
            'Date',
            'Montant',
            'Moyen de paiement',
        ]);

        $dons->each(function ($don) {
            try {
                $this->addLine([
                    $don->contact->full_name,
                    $don->formatted_date_don,
                    $don->formatted_amount,
                    $don->payment,
                ]);
            } catch (\Exception $e) {
                return ['status' => false, 'message' => $e->getMessage()];
            } finally {
                return true;
            }
        });
	}
}