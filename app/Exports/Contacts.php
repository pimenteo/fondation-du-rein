<?php

namespace App\Exports;

class Contacts extends \App\Helpers\ExportCsv
{

	public function __construct($contacts = null, $filename = 'exportContacts')
	{
		parent::__construct( $filename );

		// header
        $this->addLine([
            'Nom complet',
            'Nom',
            'Email',
            'Profession',
            'Société',
            'Téléphone 1',
            'Téléphone 2',
            'Adresse',
            'Code postal',
            'Ville',
            'Pays',
            'Invitation envoyée',
            'Invité de',
            'Mot à joindre par',
            'Carton entreprise',
            'Paiment place',
            'Catégorie',
            'Participation confirmée',
            'Total des dons',
        ]);

        $contacts->each(function ($contact) {
            try {
                $this->addLine([
                    $contact->full_name,
                    $contact->firstname,
                    $contact->email,
                    $contact->profession,
                    $contact->society,
                    $contact->telephone_1,
                    $contact->telephone_2,
                    $contact->full_address,
                    $contact->postcode,
                    $contact->city,
                    $contact->country,
                    $contact->invited_in_last_gala,
                    $contact->invite_de,
                    $contact->mot_a_joindre,
                    $contact->carton_entreprise,
                    $contact->paiement_place,
                    $contact->category,
                    $contact->confirmed_in_last_gala,
                    $contact->total_dons,
                ]);
            } catch (\Exception $e) {
                return ['status' => false, 'message' => $e->getMessage()];
            } finally {
                return true;
            }
        });
	}

}