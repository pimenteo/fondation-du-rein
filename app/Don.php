<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Don extends Model
{
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $fillable = [ 'contact_id', 'amount', 'date_don', 'payment', 'marketing' ];


	/**
     * The attributes that should be casted to native types.
     *
     * @type array
     */
    protected $dates = [
        'date_don',
        'created_at', 
        'updated_at',
    ];
	

	/***** lists *****/
	public function getPaymentTypes() 
	{
		return [
			'CB'	=> 'CB', 
			'Chèque'		=> 'Chèque', 
			'Virement'	=> 'Virement', 
			'Espèces'	=> 'Espèces', 
			'Autres'	=> 'Autres', 
		];
	}

	public function getMarketingTypes() 
	{
		return [
			'Site internet' 		=> 'Site internet', 
			'Gala' 			=> 'Gala', 
			'Evènement' 	=> 'Evènement', 
			'Prospection' 	=> 'Prospection', 
			'Autres' 		=> 'Autres', 
		];
	}


	// belongs to

	/**
     * Get the participations's contact.
     */
	public function contact() 
	{
		return $this->belongsTo('App\Contact');
	}
	

	/***** scopes *****/
	public function scopeBetweenDate($query, $dates = []) 
	{
		if( ! isset($dates['min']) || strlen($dates['min']) == 0 &&
            ! isset($dates['max']) || strlen($dates['max']) == 0) {
            return $query;
        }

        $startDate = Carbon::createFromFormat('d/m/Y', $dates['min']);
        $endDate = Carbon::createFromFormat('d/m/Y', $dates['max']);

		return $query->whereBetween('date_don', [$startDate, $endDate]);;
	}

	public function scopeBetweenDateOrder($query, $direction)
	{
		return $query->orderBy('date_don', $direction);
	}


	/***** mutators *****/
    public function setDateDonAttribute($value)
    {
        $this->attributes['date_don'] = strlen($value) > 0 ? \Carbon\Carbon::createFromFormat('d/m/Y', $value) : \Carbon\Carbon::now();
    }

    public function getFormattedDateDonAttribute()
    {
    	return $this->date_don->format('d/m/Y');
    }

    public function getFormattedAmountAttribute() 
    {
    	return $this->amount . ' €';
    }

    public function getSearchFieldset($key = null) 
    {
        $fieldset =  [
            'contact_id'     => [
                'type'  => 'equal',
                'label' => 'Contact',
                'options'	=> Contact::getList(true),
                'value' => null,
            ],
            'scope.betweenDate' => [
                'type'  => 'between-dates',
                'label' => 'Date du don',
                'value' => [
                	'min' => null,
					'max' => null,
                ],
            ],
        ];

        if (is_string($key)) {
            return array_key_exists($key, $fieldset) ? $fieldset[$key] : null;
        }
        if (is_array($key)) {
            return array_only($fieldset, $key);
        }

        return $fieldset;
    }

}
