<?php

use App\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'     => 'Nicolas Brabant',
            'email'    => 'nbr@ppgm.fr',
            'password' => Hash::make('$azerty1'),
        ]);
        User::create([
            'name'     => 'Antoine Rogé',
            'email'    => 'aro@ppgm.fr',
            'password' => Hash::make('$azerty1'),
        ]);
        User::create([
            'name'     => 'Kathleen Philippart',
            'email'    => 'kathleen@c2groupe.eu',
            'password' => Hash::make('kathleen@c2groupe.eu'),
        ]);
    }
}
