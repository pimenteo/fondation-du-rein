<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixBoolean extends Migration
{
    
    protected $tables = [
        'participations' => [ 'relance', 'carton_entreprise', 'paiement_place', 'confirmed' ],
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach ($this->tables as $tableName => $fields) {
            foreach ($fields as $fieldName) {
                $this->fixBooleanFormat($tableName, $fieldName);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }

    protected function fixBooleanFormat($table, $fieldname)
    {
        return DB::statement('ALTER TABLE `'.$table.'` CHANGE COLUMN `'.$fieldname.'` `'.$fieldname.'` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0');
    }
}
