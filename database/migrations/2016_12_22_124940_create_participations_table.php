<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParticipationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('participations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('contact_id');
            $table->integer('gala_id');
            $table->string('invite_de')->nullable();
            $table->string('service_rendu')->nullable();
            $table->boolean('relance');
            $table->enum('category', ['payant', 'invite']);
            $table->string('mot_a_joindre')->nullable();
            $table->boolean('carton_entreprise');
            $table->boolean('paiement_place');
            $table->integer('nb_place');
            $table->integer('nb_repas');
            $table->boolean('confirmed');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('participations');
    }
}
